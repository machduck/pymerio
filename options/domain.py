import framework

description = '''(domain) Target domain. Ex: site.tld'''
framework.option( 'domain', None, description )

description = '''(bool) Rerun with no filters'''
framework.option( 	'reuse',
					False,
					description,
					validate = lambda x: True if x == 0 or x == 1 else False )
