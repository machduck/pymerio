import framework

description = '''(bool) Use socket.gethostbyname() for resolving domain names
	instead of dnspython.'''
framework.option( 	'standard_ns',
					False, 
					description,
					validate = lambda x: True if x==0 or x==1 else False )

framework.option( 'ns_timeout', 30, '(int) Nameserver timeout' )

framework.option( 'ns_cooldown', 1, '(int) Nameserver cooldown' )

description = '''(ip) DNS server to use for lookups. Only available if using
	dnspython resolution method (default).'''
framework.option( 'nameserver', None, description )
