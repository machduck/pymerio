import framework

description = '''(cdir) Inverse number of bits subject to change in ip int.
	Ex: /24'''
framework.option( 	'cdir',
					'/24',
					description )
