import framework

description = '''Number of threads to use.'''
framework.option( 'threads', 0x5, description )
