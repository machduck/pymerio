import framework

description = '''(int) Recursion max depth. 0 = infinite.'''
framework.option( 	'maxdepth',
					255,
					description )

description = '''(int) Recursion depth timeout.'''
framework.option( 	'maxnulldepth',
					255,
					description )
