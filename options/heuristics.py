import framework

description = '''Use heuristics.'''
framework.option( 	'heuristics',
					True, 
					description,
					validate = lambda x: True if x==0 or x==1 else False )
