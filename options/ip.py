import framework

description = '''(ip) Target ip.
	Ex: 192.168.0.1
	Add targets to db via target command to use ranges.
	Ex: target 192.168-170.0.1-255'''
framework.option( 'ip', None, description )
