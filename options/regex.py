import framework

description = '''Ignore regex.'''
framework.option( 'ignore_regex', None, description )

description = '''Match regex.'''
framework.option( 'match_regex', None, description )
