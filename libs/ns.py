#!/usr/bin/python

import socket

try:
	import dns.resolver
except:
	pass

class ResolverAnsw(): # backwards compatibility with dnspython oop
	def __init__( self, ip ):
		self.address = ip
class StdResolver(): # backwards compatibility with dnspython oop
	def query( self, domain, type ):
		if( type != 'A' ):
			raise Exception( 'Standard resolver does not support querying '\
					'non A-type records.' )
		return [ ResolverAnsw( socket.gethostbyname( domain ) ), ]

class Resolver(): # Adapter
	def __init__( self, std = 0, ns = None ):
		self.std = std
		if( std ):
			self.resolver = StdResolver()
		else:
			self.resolver = dns.resolver.Resolver()
			if( ns is not None ):
				self.resolver.nameservers = [ ns, ]
	def query( self, domain, type = 'A' ):
		return self.resolver.query( domain, type )
	def rquery( self, ip ):
		return socket.gethostbyaddr( ip )[0]
	def lookup( self, domain ): # identical to query, xcept returns array
		return [ x.address for x in self.resolver.query( domain ) ]
	def rlookup( self, ip ): # identical to rquery, compliments lookup
		return socket.gethostbyaddr( ip )[0]
	def add_ns( self, ns ):
		if( not self.std ):
			self.resolver.nameservers.append( ns )
	def set_ns( self, ns ):
		if( not self.std ):
			self.resolver.nameservers = ns
