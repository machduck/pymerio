#!/usr/bin/python

import Queue
import threading

class ThreadMsg( Exception ):
	def __init__( self, *args, **kwargs ):
		Exception.__init__( self, *args, **kwargs )
		self.stop = True

class Launcher(): # TODO: deprecated, port all modules to multiprocessing
	def __init__( 	self, 
					target,
					amount,
					args = (),
					put_limit = 0,
					unblock = None ):
		self.master_RX = Queue.Queue()
		self.master_TX = Queue.Queue( put_limit )
		self.run_flag = threading.Event()
		self.run_flag.set()
		self.stop_flag = threading.Event()
		self._active = []
		self.first_time = True
		for i in range( 0, amount ):
			self._active.append( Thread(target,
										self.master_RX,
										self.master_TX,
										self.run_flag,
										self.stop_flag,
										args,
										unblock ) )
		for thread in self._active:
			thread.start()
	def put( self, message ):
		self.master_TX.put( message )
	def put_all( self, message ):
		for i in range( 0, len( self._active ) ):
			self.put( message )
	def stop( self ):
		for i in range( 0, len( self._active ) ):
			self.put( ThreadMsg() )
	def pause( self ):
		self.run_flag.clear()
	def resume( self ):
		self.run_flag.set()
	def hardstop( self ):
		self.stop_flag.set()
	def wait( self ):
		counter = 0
		amount = len( self._active )
		while( True ):
			message = self.master_RX.get()
			if( isinstance( message, ThreadMsg) ):
				counter = counter + 1
				if( counter == amount ):
					break
			else:
				yield( message )

class Thread( threading.Thread ):
	def __init__( 	self, 
					target, 
					m_RX,
					m_TX,
					run,
					stop,
					args,
					unblock ):
		threading.Thread.__init__( 	self ) 
		self.target = target
		self.master_RX = m_RX
		self.master_TX = m_TX
		self.run_flag = run
		self.stop_flag = stop
		self.unblock = unblock
		self.data = ''
		self.args = args
	def active( self ):
		self.data = self.master_TX.get( True, self.unblock )
		if( self.stop_flag.isSet() ):
			return False
		if( not self.stop_flag.isSet() ):
			self.run_flag.wait()
		if( isinstance(self.data, ThreadMsg) ):
			self.master_RX.put( ThreadMsg() )
			return False
		return True
	def run( self ):
		self.target( self, *self.args )
	def output( self, message ):
		self.master_RX.put( message )
	def input( self ):
		return self.data
