#!/usr/bin/python

import urllib2

def build_opener( *args, **kwargs ):
	return urllib2.build_opener( *args, **kwargs )

def socks_opener( *args, **kwargs ):
	opener = urllib2.build_opener( *args, **kwargs )
	opener.addheaders = [('User-agent', 'Mozilla/5.0')]
	return opener

def add_header( opener, header ):
	opener.addheaders.append( header )

def add_headers( opener, headers ):
	for header in headers:
		opener = add_header( opener, header )

def set_headers( opener, headers ):
	opener.addheaders = headers
