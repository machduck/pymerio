#!/usr/bin/python

import framework

from libs import thread

import urllib2
import threading
from lxml import etree

description = '''This module outputs attack surface ( optionally into file )
	sorted via TIC rankings.'''
framework.info( name = 'TIC',
				author = framework.__author__,
				description = description )
framework.option( 'file', None, 'Out file' )

description = '''Number of threads to use to fetch yandex tic ranks.'''
framework.option( 'threads', 0x5, 'Threads', description )

class Plugin( framework.Plugin ): # add xml, grep, sql [..]
	def init( self ):
		if( self.option.file is not None ):
			self.file()
		self.db = framework.db()
		self.tic_val = 0
		self.tl = thread.Launcher( 	get_tic,
									self.option.threads,
									args = ( self, ),
									put_limit = 1 )
		for domain in self.db.get_domains( tic = None ):
			self.tl.put( domain['domain'] )
		self.tl.stop()
		self.tl.wait()
		self.output( 'Rankings:' )
		self.output( '---------' )
		for domain in self.db.get_domains({ 'tic': { '$exists': True }}) \
				.sort('tic', 1 ):
			self.output( '%s => %s' % ( domain['domain'], 
										domain['tic'] ) )
			self.tic_val += domain['tic']
		self.output( '' )
		self.output( 'Total:' )
		self.output( '------' )
		self.output( 'Value: %d' % ( self.tic_val ) )
	def output( self, message ):
		if( self.option.file ):
			self.f.write( message + '\n' )
		else:
			self.out( message )
	def file( self ):
		try:
			self.f = open( self.option.file, 'wb' )
		except:
			self.error( 'Abort! Error writing to: %s' % ( self.option.file ) )

def get_tic( thread, self ):
	opener = urllib2.build_opener()
	url = 'http://bar-navig.yandex.ru/u?ver=2&lang=1049&'\
			'url=http://%s' \
			'&show=1&thc=0'
	while( thread.active() ):
		domain = thread.input()
		try:
			r = opener.open( url % domain )
			data = r.read()

			# urllib2 unicode support
			encoding=r.headers['content-type'].split('charset=')[-1]
			udata = unicode( data, encoding )

			root = etree.XML( data )
			for child in root:
				if( child.tag == 'tcy' ):
					value = child.get( 'value' )
			self.db.updateDomain( domain, tic = int(value) )
		except:
			self.error( 'Domain %s failed' % ( domain ) )
