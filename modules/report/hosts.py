#!/usr/bin/python

import framework

description = '''This module outputs attack surface ( optionally into file ).'''
framework.info( name = 'hosts',
				author = framework.__author__,
				description = description )
framework.option( 'file', None, 'Out file' )

class Plugin( framework.Plugin ): # add xml, grep, sql [..]
	def init( self ):
		if( self.option.file is not None ):
			self.file()
		db = framework.db()
		self.output( 'IP Addresses:' )
		self.output( '-------------' )
		for ip in db.getIPs():
			self.output( ip['ip'] )
		self.output( '' )
		self.output( 'Domains:' )
		self.output( '--------' )
		for domain in db.getDomains():
			ips = db.getIPAddressesByDomain( domain = domain['domain'] )
			self.output( '%s => %s' % ( domain['domain'], ','.join( ips ) ) )
		self.output( '' )
		self.output( 'Total:' )
		self.output( '------' )
		ips = db.getIPs().count()
		domains = db.getDomains().count()
		self.output( 'IPs: %d' % ( ips ) )
		self.output( 'Domains: %d' % ( domains  ) )
	def output( self, message ):
		if( self.option.file ):
			self.f.write( message + '\n' )
		else:
			self.out( message )
	def file( self ):
		try:
			self.f = open( self.option.file, 'wb' )
		except:
			self.error( 'Abort! Error writing to: %s' % ( self.option.file ) )
