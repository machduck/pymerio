#!/usr/bin/python

import framework

from libs import thread

import httplib
import threading
import time

description = '''This module outputs attack surface ( optionally into file )
	sorted via PR rankings.'''
framework.info( name = 'PR',
				author = framework.__author__,
				description = description )
framework.option( 'file', None, 'Out file' )

description = '''Number of threads to use to fetch google page ranks.'''
framework.option( 'threads', 0x5, 'Threads', description )

description = '''Cooldown between requests for each thread'''
framework.option( 'cooldown', 0x1, description )

class Plugin( framework.Plugin ): # add xml, grep, sql [..]
	def init( self ):
		if( self.option.file is not None ):
			self.file()
		self.db = framework.db()
		self.pr_total = 0
		self.tl = thread.Launcher( 	get_pr,
									self.option.threads,
									args = ( self, ),
									put_limit = 1 )
		for domain in self.db.get_domains( pr = None ): # awesome
			self.tl.put( domain['domain'] )
		self.tl.stop()
		self.tl.wait()
		self.output( 'Rankings:' )
		self.output( '---------' )
		for domain in self.db.getDomains({ 'pr': { '$exists': True }}) \
				.sort('pr', 1):
			self.output( '%s => %s' % ( domain['domain'], domain['pr'] ) )
			self.pr_total += domain['pr']
		self.output( '' )
		self.output( 'Total:' )
		self.output( '------' )
		self.output( 'PR: %d' % ( self.pr_total ) )
	def output( self, message ):
		if( self.option.file ):
			self.f.write( message + '\n' )
		else:
			self.out( message )
	def file( self ):
		try:
			self.f = open( self.option.file, 'wb' )
		except:
			self.error( 'Abort! Error writing to: %s' % ( self.option.file ) )

def get_hash( page ):
	seed = "Mining PageRank is AGAINST GOOGLE'S TERMS OF SERVICE."\
			"Yes, I'm talking to you, scammer."
	hash = 0x01020345
	for i in range( len( page )):
		hash = hash ^ ord( seed[ i % len( seed ) ]) ^ ord( page[ i ] )
		hash = hash >> 23 | hash << 9
		hash = hash & 0xffffffff
	return '8%x' % hash

def get_pr( thread, self ):
	while( thread.active() ):
		domain = thread.input()
		url = 'toolbarqueries.google.com'
		query = '/tbr?client=navclient-auto&ch=%s&features=Rank&q=info:%s'
		try:
			req = httplib.HTTPConnection( url )
			hash = get_hash( domain )
			path = query % ( hash, domain )
			req.request("GET", path)
			r = req.getresponse()
			data = r.read()
			req.close()
			rank = data.split(":")[-1].strip()
			if( rank != '' ):
				self.db.updateDomain( domain, pr = int( rank ) )
			else:
				self.warn( 'Domain %s has no pr' % ( domain ) )
				self.db.updateDomain( domain, pr = 0 )
		except:
			self.error( 'Domain %s failed' % ( domain ) )
		finally:
			time.sleep( self.option.cooldown )
