#!/usr/bin/python

import framework
import subprocess

from libs import thread

description = '''This module executes arbitrary os commands using db output.'''
framework.info( name = 'oscmd',
				author = framework.__author__,
				description = description )

description = '''Command to execute. 
	%DOMAIN% will be replaced with a domain name.
	%IP% will be replaced with an ip address.
	Ex: echo "%IP% :: %DOMAIN%" >> ~/db.txt
	Ex: /pentest/web/whatweb/whatweb -a 3 %DOMAIN% >> ~/enum.txt
	Ex: /pentest/web/arachni/bin/arachni -c -f -g -p http://%DOMAIN%'''

framework.option( 'cmd', None, description, required = True )

description = '''IP address filter to use. 
	Ex: "{'ip': '10.0.0.1'}"'''
framework.option( 'ip_filter', '{}', description )

description = '''Domain name filter to use.
	Ex: "{'domain': '10.0.0.1'}"'''
framework.option( 'domain_filter', '{}', description )

description = '''Custom ip update command to run after command completed.
	Ex: "{'cmd': True}"'''
framework.option( 'ip_update', None, description )

description = '''Custom domain update command to run after command completed.
	Ex: "{'cmd': True}"'''
framework.option( 'domain_update', None, description )

description = '''Use sudo (bool)'''
framework.option( 	'sudo',
					False,
					description,
					validate = lambda x: True if x == 0 or x== 1 else False )

description = '''Password for sudo. WARNING! PASSWORD IS VISIBLE!'''
framework.option( 'passwd', None, description )

description = '''Number of threads to use for running commands.'''
framework.option( 'threads', 0x1, description )

class Module( framework.Module ):
	def init( self ):
		check = self.validate_options()
		if( not check ):
			self.error( 'Option %s not set' % ( check ) )
			return
		self.success( 'Started' )
		self.db = framework.db()
		self.oscmd()
		self.success( 'Finished' )
	def stop( self ):
		self.tl.hardstop()
	def pause( self ):
		self.tl.pause()
	def resume( self ):
		self.tl.resume()
	def oscmd( self ):
		self.tl = thread.Launcher( 	oscmd,
									self.option.threads,
									args = ( self, ),
									put_limit = 1 )
		input = False
		if( self.option.cmd.find( '%IP%' ) > -1 ):
			ipf = True
		else:
			ipf = False
		if( self.option.cmd.find( '%DOMAIN%' ) > -1 ):
			domainf = True
		else:
			domainf = False

		if( ipf == False and domainf == False ):
			self.warn( 'No database value usage in cmd detected.' )

		if( self.option.domain_filter is not None ):
			d_key = eval( self.option.domain_filter )
		else:
			d_key = {}

		if( self.option.ip_filter is not None ):
			i_key = eval( self.option.ip_filter )
		else:
			i_key = {}

		if( ipf and not domainf ):
			ip_g = self.db.getIPAddresses
			domain_g = dummy_generator
		elif( domainf and not ipf ):
			domain_g = self.db.getDomainNames
			ip_g = dummy_generator
		elif( domainf and ipf ):
			ip_g = self.db.getIPAddresses
			domain_g = self.db.getDomainNames

		for ip in ip_g():
			for domain in domain_g():
				input = True
				cmd = self.option.cmd
				cmd = cmd.replace( '%DOMAIN%', domain )
				cmd = cmd.replace( '%IP%', ip )
				self.tl.put( cmd )
				if( ipf and self.option.ip_update is not None ):
					self.db.extUpdateIP({ 'ip': ip },
										eval( self.option.ip_update ) )
				if( domainf and self.option.domain_update is not None ):
					self.db.extUpdateDomain({ 'domain': domain },
											eval( self.option.domain_update ) )
		if( not input ):
			self.warn( 'No input' )
		self.tl.stop()
		self.tl.wait()

def dummy_generator( *args, **kwargs ):
	yield ''

def oscmd( thread, self ):
	while( thread.active() ):
		input = thread.input()
		if( self.option.sudo ):
			cmd = 'echo %s | sudo -S %s' % ( self.option.passwd, input )
		else:
			cmd = input
		try:
			self.info( 'Running %s' % ( cmd ) )
			subprocess.call( cmd, stderr = subprocess.STDOUT, shell = True )
		except:
			self.warn( 'Failed: %s' % ( cmd ) )
