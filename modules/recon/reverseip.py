#!/usr/bin/python

import framework
from libs import thread
from libs import ns as dns

import time
import socket

description = '''This module maps ip addresses to domain names via their
	PTR records with verification.'''
framework.info( name = 'reverseip',
				author = framework.__author__,
				description = description )

description = '''To lookup specific ip set this option.
	Ex: 192.168.0.1
	Add targets to db via target command to use ranges.
	Ex: target 192.168-170.0.1-255'''
framework.option( 'ip', None, 'IP', description )

description = '''Number of threads to use for reverseip lookups.'''
framework.option( 'threads', 0x5, 'Threads', description )

description = '''DNS server to use for lookups. Only available if using
	dnspython resolution method (default).'''
framework.option( 'ns', None, 'DNS server', description )

description = '''Resolve ip addresses that already map to domains.'''
framework.option( 	're_resolve',
					0x0, 
					'Re-resolve ip addresses (bool)',
					description,
					lambda x: True if x == 0 or x == 1 else False )

framework.option( 'socket_timeout', 30, 'Socket timeout' )

description = '''Use socket.gethostbyname() for resolving domain names
	instead of dnspython.'''
framework.option( 	'standard_dns', 0x0, 'Standard DNS (bool)', description,
					lambda x: True if x == 0 or x == 1 else False )

description = '''DNS server timeout.'''
framework.option( 'ns_timeout', 30, 'NS cooldown', description )

description = '''DNS server wait time between requests.'''
framework.option( 'ns_cooldown', 0x1, 'NS timeout', description )

class Module( framework.Module ):
	def init( self ):
		check = self.validate_options()
		if( not check ):
			self.error( 'Option %s not set.' % ( check ) )
			return
		self.success( 'Started' )
		self.db = framework.db()
		if( self.option.ip is None ):
			if( self.option.re_resolve ):
				self.ips = self.db.getIPAddresses()
			else:
				self.ips = self.db.getIPAddresses( reverseip = None )
		else:
			self.db.ip( self.option.ip )
			self.ips = [ self.option.ip ]
		self.resolver = dns.Resolver( self.option.standard_dns, self.option.ns )
		self.resolver.timeout = self.option.ns_timeout
		socket.setdefaulttimeout( self.option.socket_timeout )
		self.reverseip()
	def stop( self ):
		self.info( 'Stopping' )
		self.tl.hardstop()
	def pause( self ):
		self.info( 'Pausing' )
		self.tl.pause()
	def resume( self ):
		self.info( 'Resuming' )
		self.tl.resume()
	def reverseip( self ):
		self.tl = thread.Launcher( 	reverseip,
									self.option.threads,
									args=( self, ),
									put_limit = 1 )
		input = False
		for ip in self.ips:
			input = True
			if( not self.active() ):
				self.exit()
			self.tl.put( ip )
		self.tl.stop()
		self.tl.wait()
		if( not input ):
			self.warn( 'No input' )
		self.success( 'Finished' )

def reverseip( thread, self ):
	while( thread.active() ):
		ip = thread.input()
		try:
			domain = self.resolver.rquery( ip )
			try:
				result = self.resolver.query( domain )
				if( ip in [ x.address for x in result ] ):
					self.info( 'IP %s maps to domain %s' % ( ip, domain ) )
					self.db.setDomain( domain )
					self.db.updateIP( ip, ptr = domain )
					self.db.mapHosts( ip, domain )
				else:
					self.info( 'IP %s does not map to domain %s' % ( ip, 
																domain ) )
			except:
				self.info( 'Error resolving %s to ip %s'  % ( domain, ip ) )
		except:
			self.info( 'Error querying PTR record for %s' % ( ip ) )
		finally:
			self.db.updateIP( ip, reverseip = True )
			self.sleep( self.option.ns_cooldown )
