#!/usr/bin/python

import framework

import socket
import time
import urllib
import urllib2
import cookielib
import json
import re
import urlparse
import itertools

from libs import thread
from libs import ns as dns

bool_check = lambda x: True if x == 0 or x == 1 else False

description = '''This module maps ip addresses to domains via online
	databases.'''
framework.info( name = 'cohost',
				author = framework.__author__,
				description = description )

description = '''Target. By default takes all domains from database.'''
framework.option( 'domain', None, 'Target', description )

description = '''Target. By default takes all domains from database.'''
framework.option( 'ip', None, 'Target', description )

description = '''Number of threads to use to verify ip to hostname mappings.'''
framework.option( 'threads', 0x5, 'Verification threads', description )

description = '''DNS server cooldown between requests.'''
framework.option( 'ns_cooldown', 0x1, 'NS cooldown', description )

framework.option( 'ns_timeout', 30, 'NS timeout' )

description = '''DNS server to use for lookups. Only available if using
	dnspython resolution method (default).'''
framework.option( 'ns', None, 'DNS server', description )

description = '''Resolve domains that already map to ip addresses.'''
framework.option( 	're_resolve',
					0x0,
					'Re-resolve domain names (bool)',
					description,
					bool_check )

description = '''Socket timeout. Used in HTTP requests.'''
framework.option( 'socket_timeout', 30, 'Socket timeout', description )

description = '''Use socket.gethostbyname() for resolving domain names
	instead of dnspython.'''
framework.option( 	'standard_dns',
					0x0, 
					'Standard DNS (bool)', 
					description, 
					bool_check )

description = '''Timeout to sleep for between consecutive domain requests from
	online databases.'''
framework.option( 'onlinedb_cooldown', 0x5, 'OnlineDB timeout',	description )

description = '''Use IPs instead of DNS adresses from database.'''
framework.option( 'use_ips', 0x0, 'Use IPs (bool)', description, bool_check )

framework.option( 'db1', 0x1, 'dnsdigger.com (bool)', validate = bool_check )
framework.option( 'db2', 0x1, 'yougetsignal.com (bool)', validate = bool_check )
framework.option( 'db3', 0x0, 'webhosting.info [inactive] (bool)', validate = bool_check )
framework.option( 'db4', 0x0, 'wservices.ru (bool)', validate = bool_check )
framework.option( 'db5', 0x0, 'spys.ru [ratelimit] (bool)', validate = bool_check )

framework.option( 	'subdomains',
					0x0,
					'lookup subdomains (bool)',
					validate = bool_check )

class ResolveCheck():
	def __init__( self, domain, ips ):
		self.domain = domain
		self.ips = ips

class Module( framework.Module ):
	def init( self ):
		check = self.validate_options()
		if( not check ):
			self.error( 'Option %s not set.' % ( check ) )
			return
		self.success( 'Started' )
		self.db = framework.db()
		if( self.option.domain is None and self.option.ip is None):
			if( self.option.use_ips ):
				if( self.option.re_resolve ):
					if( self.option.subdomains ):
						self.domains = self.db.getIPAddresses()
					else:
						self.domains=self.db.getIPAddressesByDomain(
								subdomain=None
						)
				else:
					if( self.option.subdomains ):
						self.domains = itertools.chain( 
								self.db.getIPAddressesByDomain( cohost=None ),
								self.db.getIPAddresses( domains=None )
						)
					else:
						self.domains = itertools.chain( 
								self.db.getIPAddressesByDomain( subdomain=None,
																cohost=None ),
								self.db.getIPAddresses( domains=None )
						)
			else:
				if( self.option.re_resolve ):
					if( self.option.subdomains ):
						self.domains = self.db.getDomainNames()
					else:
						self.domains = self.db.getDomainNames( subdomain=None )
				else:
					if( self.option.subdomains ):
						self.domains = self.db.getDomainNames( cohost = None )
					else:
						self.domains = self.db.getDomainNames( subdomain = None,
															cohost = None )
		else:
			if( self.option.domain is not None ):
				self.db.setDomain( self.option.domain )
				self.domains = [ self.option.domain, ]
			if( self.option.ip is not None ):
				self.db.setIP( self.option.ip )
				self.domains = [ self.option.ip, ]
		self.resolver = dns.Resolver( self.option.standard_dns, self.option.ns )
		self.resolver.timeout = self.option.ns_timeout
		socket.setdefaulttimeout( self.option.socket_timeout )
		self.onlinedb()
	def stop( self ):
		self.tl.hardstop()
	def pause( self ):
		self.tl.pause()
	def resume( self ):
		self.tl.resume()
	def onlinedb( self ):
		self.tl = thread.Launcher( 	check_domain,
									self.option.threads,
									args = ( self, ),
									put_limit = 1 )
		input = False
		for domain in self.domains:
			input = True # generators dont know their length
			if( not self.active() ):
				self.exit()
			try:
				if( self.option.use_ips or self.option.ip is not None ):
					# if we have ip
					res_obj = [ dns.ResolverAnsw( domain ), ]
				else: # if we have domain
					res_obj = self.resolver.query( domain )
				if( self.option.db1 ):
					self.db1( domain, res_obj )
				if( self.option.db2 ):
					self.db2( domain, res_obj )
				if( self.option.db3 ):
					self.db3( domain, res_obj )
				if( self.option.db4 ):
					self.db4( domain, res_obj )
				if( self.option.db5 ):
					self.db5( domain, res_obj )
				if( not self.option.use_ips or self.option.domain is not None ):
					# if we have domain
					self.db.updateDomain( domain, cohost = True )
				else: # if we have ip
					domains = self.db.getDomainNamesByIP( ip = domain ) # alpha
					for domain in domains:
							self.db.updateDomain( 	domain,
													cohost = True )
			except:
				import traceback; traceback.print_exc()
				self.warn( 'Failed looking up domain %s' % ( domain ) )
				continue
			time.sleep( self.option.onlinedb_cooldown )
		self.tl.stop()
		self.tl.wait()
		if( not input ):
			self.warn( 'No input' )
		self.success( 'Finished' )

	def db1( self, domain, ips ):
		site = 'dnsdigger.com'
		url_token ='http://www.dnsdigger.com/'
		url_lookup='http://www.dnsdigger.com/hostcollision.php?host=%s&token=%s'
		headers = { 'Referer': 'http://www.dnsdigger.com/',
					'User-Agent': 'Mozilla/5.0'	}

		try:
			cj = cookielib.CookieJar()
			opener = urllib2.build_opener( urllib2.HTTPCookieProcessor( cj ) )
			self.addheaders( headers, opener )
			r = opener.open( url_token )
			r = r.read()
			r = re.search( 	'name="token"\svalue="(?P<token>.*?)">', 
							r, 
							re.U and re.M )
			token = r.group('token') # first-try r3g3x n1nj4 ach13v3d
			r = opener.open( url_lookup % ( domain, token ) )
			r = r.read()
		except urllib2.HTTPError as e:
			self.warn('%s %s failed mapping %s'%( e.getcode(), site, domain ) )
			return

		r = re.findall( '<a\shref=".*?"\srel="nofollow">(.*?)</a>',
						r, 
						re.U and re.M )
		for domain in r:
			self.tl.put( ResolveCheck( domain, ips ) )

	def db2( self, domain, ips ):
		site = 'yougetsignal.com'
		url = 'http://www.yougetsignal.com/tools/web-sites-on-web-server'\
				'/php/get-web-sites-on-web-server-json-data.php'
		post = { 'remoteAddress': domain, 'key': '' }
		headers = { 'X-Requested-With': 'XMLHttpRequest', 
					'X-Prototype-Version': '1.6.0',
					'Referer': 'http://www.yougetsignal.com/tools'\
							'/web-sites-on-web-server/',
					'User-Agent': 'Mozilla/5.0'	}

		try:
			r = self.request( url, post, headers )
		except urllib2.HTTPError as e:
			self.warn('%s %s failed mapping %s'%( e.getcode(), site, domain ) )
			return
		r = json.loads( r )

		if( r['status'] == 'Success' ):
			self.info('%s found %s domains mapping to host %s on ip %s' % (
					site, r['domainCount'], domain, r['remoteIpAddress'] ) )
		else:
			self.error( '%s failed at looking up %s' % ( site, domain ) )
			return
		if( r['remoteIpAddress'] not in [x.address for x in ips] ):
			self.error( '%s failed looking up %s: ip mismatch.' % (
														site,
														domain ) )
			return

		for domain in r['domainArray']:
			self.tl.put( ResolveCheck( domain[0], ips ) )

	def db3( self, domain, ips ):
		return # XXX site timeout
		site = 'webhosting.info'
		url = 'http://whois.webhosting.info/'
		headers = { 'User-Agent': 'Mozilla/5.0' }

		domain_ips = [ x.address for x in ips ]
		urls = [ url + x for x in domain_ips ]

		for urlz in urls:
			base_url = urlz
			url = base_url
			counter = 1
			while( True ):
				self.info( 'Page number: %d' % ( counter ) )
				if( counter > 1 ):
					param = '?pi=%d&ob=SLD&oo=ASC' % ( counter )
					url = base_url + param
				try:
					r = self.request( url, {}, headers )
				except urllib2.HTTPError as e:
					self.warn( '%s %s failed looking up %s' % ( e.getcode(),
																site,
																domain ))
					break

				self.info( 'Requesting %s' % ( url ) )

				r = re.findall( '<td><a\shref=(?:.*?)>(.*?)\.<\/a><\/td>',
								r, 
								re.M and re.M )
				if( len( r ) == 0 ):
					self.error( '%s finished or is blocking us' % ( site ) )
					break

				for domain in r:
					self.tl.put( ResolveCheck( domain.lower(), ips ) )
				counter = counter + 1
				time.sleep(7)

	def db4( self, domain, ips ):
		site = 'wservices.ru'
		url = 'http://wservices.ru/reverseip.php'
		post = { 	'domain_input': domain, 
					'type': '3', 
					'ext':'', 
					'do_global':'0' }
		headers = { 'Referer': 'http://wservices.ru/reverseip.php',
					'User-Agent': 'Mozilla/5.0'	}

		try:
			cj = cookielib.CookieJar()
			opener = urllib2.build_opener( urllib2.HTTPCookieProcessor( cj ) )
			self.addheaders( headers, opener )
			r = opener.open( url, urllib.urlencode( post ) )
			r = r.read()
			r = opener.open( url )
			r = r.read()
		except urllib2.HTTPError as e:
			self.warn('%s %s failed mapping %s'%( e.getcode(), site, domain ) )
			return

		r = re.findall( '[0-9]+\.\s<a.*?>(.*?)<\/a>.*?<br>', r, re.U and re.M )
		for domain in r:
			self.tl.put( ResolveCheck( domain, ips ) )

	def db5( self, domain, ips ):
		site = 'spys.ru'
		url = 'http://spys.ru/ips/'
		post = { 'iphost': domain, 'ipsr': '2' }
		headers = { 'Referer': 'http://spys.ru/ips/',
					'User-Agent': 'Mozilla/5.0 (compatible; MSIE 9.0;'\
							'Windows NT 6.1; Trident/5.0)' }

		try:
			r = self.request( url, post, headers )
		except urllib2.HTTPError as e:
			self.warn('%s %s failed looking up %s'%( e.getcode(),site,domain ))
			return

		r = re.findall( '<font\sclass=spy2>(.*?)</font>', r, re.M and re.M )
		if( len( r ) == 0 ):
			self.error( '%s is probably blocking us, try another service.' % ( 
																		site ) )
			return

		for http in r:
			parts = urlparse.urlparse( http )
			domain = parts.netloc.split(':')[0]
			self.tl.put( ResolveCheck( domain, ips ) )

	def request( self, url, post, headers ):
		r = urllib2.Request(url, 
							urllib.urlencode( post ),
							headers )
		r = urllib2.urlopen( r )
		r = r.read()
		return r

	def addheaders( self, headers, opener ):
		li = []
		for header in headers.keys():
			li.append( (header, headers[ header ] ) )
		opener.addheaders = li
		return opener

def check_domain( thread, self ):
	if( self.option.ns_cooldown is None ):
		timeout = 1
	else:
		timeout = self.option.ns_cooldown
	while( thread.active() ):
		rcheck = thread.input()
		try:
			ips = self.resolver.query( rcheck.domain )
			if( len( [ x.address for x in rcheck.ips 
						if( x.address in [y.address 
							for y in ips ] ) ] ) != 0 ):
				self.info('Domain %s passed verficication.' % ( rcheck.domain ))
				self.db.setDomain( rcheck.domain, cohost = True )
				for ip in ips:
					self.db.setIP( ip.address )
					self.db.mapHosts( ip.address, rcheck.domain )
			else:
				self.warn( 'Domain %s failed verification.' % ( rcheck.domain ))
		except:
			pass
		finally:
			time.sleep( timeout )
