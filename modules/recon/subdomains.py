#!/usr/bin/python

import framework
from libs import thread
from libs import ns as dns

import time
import socket
import os
import urllib2
import difflib # 0x5f3759df

description = '''This module brutes subdomains.'''
framework.info( name = 'subdomains',
				author = framework.__author__,
				description = description )

description = '''Default argument takes as input all domains in db.
	Ex: site.tld'''
framework.option( 'domain', None, 'Domain name', description )

description = '''Number of threads to use for resolving domain names.'''
framework.option( 'threads', 0x5, 'Threads', description )

description = '''DNS server timeout between requests.'''
framework.option( 'ns_cooldown', 0x1, 'NS cooldown', description )

framework.option( 'ns_timeout', 30, 'NS timeout' )

framework.option( 'socket_timeout', 30, 'Socket timeout' )

description = '''Use socket.gethostbyname() for resolving domain names
	instead of dnspython.'''
framework.option( 'standard_dns', 0x0, 'Standard DNS (bool)', description,
					lambda x: True if x == 0 or x == 1 else False )

description = '''DNS servers to use for lookups. Only available if using
	dnspython resolution method (default). Seperate with pipe.
	Ex: set ns "192.168.0.1|8.8.8.8|8.8.4.4"'''
framework.option( 'nss', None, 'DNS server', description )


description = '''Word list to use. Consider:
	- monad.wl
	- fierce.wl
	- knock.wl
	- cash.rdot.wl'''
framework.option( 'wordlist', 'subdomains.wl', 'Wordlist', description )

description = '''Simularity metric ratio at which wildcard domain 
	index pages will be considered identical and subdomain will be ignored.'''
framework.option( 'ratio', 0.9, 'Simularity ratio', description )

framework.option( 'http_timeout', 30, 'HTTP timeout for wildcards' )

framework.option( 	'subdomains',
					0x0,
					'Brute all not bruted domains in db (bool)',
					validate = lambda x: True if x == 0 or x == 1 else False )

framework.option( 're_brute', 0x0, 'Brute already bruted (bool)',
					validate = lambda x: True if x == 0 or x == 1 else False )

class Module( framework.Module ):
	def gen_target( self ):
		if( self.option.domain is None ):
			if( self.option.re_brute ):
				if( self.option.subdomains ):
					self.domains = self.db.getDomainNames()
				else:
					self.domains = self.db.getDomainNames( subdomain = None )
			else:
				if( self.option.subdomains ):
					self.domains = self.db.getDomainNames( bruteforced = None )
				else:
					self.domains = self.db.getDomainNames( 	bruteforced = None,
															subdomain = None )
		else:
			self.db.setDomain( self.option.domain )
			self.domains = [ self.option.domain, ]
	def init( self ):
		check = self.validate_options()
		if( not check ):
			self.error( 'Option %s not set.' % ( check ) )
			return
		self.success( 'Started' )
		self.db = framework.db()
		self.gen_target()
		socket.setdefaulttimeout( self.option.socket_timeout )
		self.resolver = dns.Resolver( self.option.standard_dns )
		self.resolver.timeout = self.option.ns_timeout
		if( self.option.nss is not None ):
			self.resolver.set_ns( self.option.nss.split('|') )
		self.brute()
	def stop( self ):
		self.tl.stop()
	def pause( self ):
		self.tl.pause()
	def resume( self ):
		self.tl.resume()
	def wildcard( self, domain ):
		# check for domain wildcard
		fake = 'thisdoesnotexist'
		try:
			ips = self.resolver.query( fake + '.' + domain )
			if( len( [ x.address for x in ips ] ) > 0 ):
				self.info( 'Wildcards in use for domain: %s' % ( domain ) )
				return True # fuck
		except:
			self.info( 'No wildcard in use for domain: %s' % ( domain ) )
			return False
	def setTLD( self, domain ):
		sd = domain.split('.')
		host = ".".join( len( sd[-2]) < 4 and sd[-3:] or sd[-2:] )
		if( host != domain ):
			self.info( 'Heuristically adding top level domain %s for %s' %(host,
																		domain))
			self.db.setDomain( host ) #,cohost = True )
			self.db.updateDomain( domain, subdomain = True )
	def brute( self ):
		for domain in self.domains:
			self.setTLD( domain )
		self.gen_target() # reset generators
		self.wildcards = {}
		self.ignore = []
		self.tl = thread.Launcher( 	brute,
									self.option.threads,
									args = ( self, ),
									put_limit = 1 )
		for domain in self.domains:
			if( not self.active() ):
				self.exit()
			if( self.wildcard( domain ) ): # get wildcarded page
				try:
					self.opener = urllib2.build_opener()
					self.opener.addheaders = [('User-agent', 'Mozilla/5.0')]
					url = 'http://' + domain
					req = self.opener.open( url,None,self.option.http_timeout )
					data = req.read()
					self.wildcards[ domain ] = {} 
					self.wildcards[ domain ]['html'] = data
				except:
					self.warn( 'Excluding domain %s from subdomain brute: '\
							'no web server.' % ( domain ) )
					self.ignore.append( domain )
		self.gen_target()
		input = False
		for domain in self.domains:
			input = True
			path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
								'..',
								'..',
								'misc',
								'wordlists',
								self.option.wordlist )
			f = open( path, 'rb' )
			for word in f:
				if( not self.active() ):
					self.exit()
				subdomain = word.strip()
				self.tl.put( subdomain + '.' + domain )
			f.close()
			self.db.updateDomain( domain, bruteforced = True )
		self.tl.stop()
		self.tl.wait()
		if( not input ):
			self.warn( 'No input' )
		self.success( 'Finished' )

def brute( thread, self ):
	while( thread.active() ):
		domain = thread.input()
		try:
			base_domain  = '.'.join(domain.split('.')[1:])
			if( base_domain in self.ignore ):
				continue
			ips = self.resolver.query( domain ) # double
			new_ips = [ x.address for x in ips ]
			if( base_domain in self.wildcards.keys() ):
				if( len( new_ips ) > 0 ): # execute costly http check
					try:
						r = self.opener.open( 	'http://' + domain, 
												None, 
												self.option.http_timeout )
						data = r.read()
						ratio = difflib.SequenceMatcher( 		None,
								self.wildcards[ base_domain ]['html'],
																data ).ratio()
						self.info('Wildcard domain %s: %0.2f vs %0.2f' % ( 	
																		domain,
																		ratio,
															self.option.ratio ))
						if( ratio < self.option.ratio ): # new page
							self.db.setDomain( 	domain,
												#cohost = True, 
												subdomain = True )
							for ip in ips:
								self.db.setIP( ip.address )
								self.db.mapHosts( ip.address, domain )
					except urllib2.HTTPError, e:
						self.warn('Domain %s error: %s' % ( domain, e.code ) )
					except socket.timeout, e:
						self.warn('Timeout for domain %s' ( domain ) )
					except:
						self.warn('Domain %s lost web server! '\
								'Manual check suggested.' % ( domain ) )
					for ip in ips:
						self.db.setIP( ip.address )
			else: # if domain is without wildcards
				if( len( [ x.address for x in ips ] ) > 0 ):
					self.db.setDomain( 	domain, 
										#cohost = True,
										subdomain = True )
				for ip in ips:
					self.info('Domain %s resolves to %s' % (domain,ip.address))
					self.db.setIP( ip.address )
					self.db.mapHosts( ip.address, domain )
		except:
			pass
		finally:
			time.sleep( self.option.ns_cooldown )
