#!/usr/bin/python

import framework

from libs import ns 
from libs import thread

from options import thread as _thread
from options import dns
from options import socket as _socket
from options import regex
from options import heuristics
from options import recursion
from options import domain

import multiprocessing
import socket
import re
import ipaddr
import math

from threading import Lock

description = '''This module guesses subnets by domain names and PTR records.'''
framework.info( name = 'subnets',
				author = framework.__author__,
				description = description )

description = '''Match existing domains against scanned.'''
framework.option( 'match_existing', True, 'Match existing', description )

def thread_hook( thread, self ):
	while( thread.active() ):
		input = thread.input()
		self.action_hook( input )

class Module( framework.Module ):
	def init( self ):
		check = self.validate_options()
		if( not check ):
			self.error( 'Option %s error: %s' % ( check, check.msg ) )
			return
		self.success( 'Module initialized' )
		self.db = framework.db()
		input = False
		socket.setdefaulttimeout( self.option.socket_timeout )
		self.resolver = ns.Resolver(self.option.standard_ns,
									self.option.nameserver )
		self.resolver.timeout = self.option.ns_timeout

		self.tl = thread.Launcher(  thread_hook,
									self.option.threads,
									args=( self, ),
									put_limit = 1 )
		self.lock = Lock()
		self.matches = set()
		if self.option.match_regex is not None:
			self.matches.append( re.compile( self.option.match_regex ))
		if self.option.match_existing:
			for domain in self.db.getDomainNames():
				regex = '.*' + re.escape(getHLD( domain ))
				self.matches.add( re.compile( regex ))
		for target in self.targets():
			input = True
			if( not self.active() ):
				self.exit()
			self.tl.put( target )
		self.tl.stop()
		self.tl.wait()
		if( not input ):
			self.warn( 'No input' )
		self.success( 'Module done' )
	def stop( self ):
		self.tl.hardstop()
	def pause( self ):
		self.tl.pause()
	def resume( self ):
		self.tl.resume()

	def targets( self ):
		if( self.option.reuse ):
			kwargs = {'subnets': None}
		else:
			kwargs = {}
		if( self.option.domain is not None ):
			if( self.option.heuristics ):
				tld = getTLD( self.option.domain )
				self.db.setDomain( tld )
				target = tld
			else:
				target = self.option.domain
			self.db.setDomain( self.option.domain )
			ips = self.resolver.lookup( target )
			for ip in ips:
				self.db.setIP( ip )
		else:
			ips = self.db.getIPAddressesByDomain( **kwargs )
		scanned = []
		mask = '/' + str(int(32-math.ceil(math.log( max([ 
				self.option.maxdepth 
			]), 2 ))))
		for ip in ips:
			ipb = ipaddr.IPv4Address( ip )
			skip = False
			for net in scanned:
				if ipb in net:
					skip = True
				else:
					self.info( '{} not in {}'.format( ipb, net ))
			if( not skip ):
				for target in self.traverse( ip ):
					yield( target )
				self.info( 'ip+mask {}'.format( ip + mask ))
				scanned.append( ipaddr.IPv4Network( ip + mask ))

	def traverse( self, ip ):
		addr = ipaddr.IPv4Address( ip )
		oaddr = ipaddr.IPv4Address( ip )
		yield( addr )
		self.info( 'Traversing up from: %s' % ( addr ) )
		iteration = 0
		operation = 1
		while( True ):
			addr += 1*operation
			yield( addr )
			iteration += 1
			if( iteration == self.option.maxdepth ):
				self.info( 'Traversing down from: %s' % ( oaddr ) )
				addr=oaddr
				operation = -1
			if( iteration == self.option.maxdepth*2 ):
				self.info( 'Option maxdepth reached' )
				break
		self.info( '%d PTR records traversed' % iteration )

	def action_hook( self, ip ):
		try:
			ptr = self.resolver.rlookup( str(ip) )
			self.info( '%s has PTR %s' % ( ip, ptr ))
			for match in self.matches:
				if( bool( match.match( ptr )) ):
					self.info( '%s matched %s' % ( ptr, match.pattern ))
					str_ip = str( ip )
					self.db.setDomain( ptr )
					self.db.setIP( str_ip )
					self.db.mapHosts( str_ip, ptr )
					regex = '.*'+re.escape(getHLD( ptr ))
					with self.lock:
						self.matches.add( re.compile( regex ))
		except:
			self.warn( 'Error looking up PTR for %s' % ( ip ) )

def getHLD( domain ):
	hld_p = domain.split('.')
	if( len(hld_p[1:]) > 1 ):
		return '.'.join( hld_p[1:] )
	else:
		return domain

def getTLD( domain ):
	sd = domain.split('.')
	tld = ".".join( len( sd[-2]) < 4 and sd[-3:] or sd[-2:] )
	return tld
