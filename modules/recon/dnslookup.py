#!/usr/bin/python

import framework
from libs import thread
from libs import ns as dns

import time
import socket

description = '''This module resolves dns addresses.'''
framework.info( name = 'dnslookup',
				author = framework.__author__,
				description = description )

description = '''Default argument takes as input all domains in db.
	Ex: site.tld'''
framework.option( 'domain', None, 'Domain name', description )

description = '''Number of threads to use for resolving domain names.'''
framework.option( 'threads', 0x5, 'Threads', description )

description = '''DNS server wait time between requests.'''
framework.option( 'ns_cooldown', 0x1, 'NS cooldown', description )

framework.option( 'ns_timeout', 10, 'NS timeout' )

description = '''DNS server to use for lookups. Only available if using
	dnspython resolution method (default).'''
framework.option( 'ns', None, 'DNS server', description )

description = '''Resolve domains that already map to ip addresses.'''
framework.option( 	're_resolve', 
					0x0, 
					'Re-resolve domain names (bool)',
					description,
					lambda x: True if x == 0 or x == 1 else False )

framework.option( 'socket_timeout', 30, 'Socket timeout' )

description = '''Use socket.gethostbyname() for resolving domain names
	instead of dnspython.'''
framework.option( 	'standard_dns', 0x0, 'Standard DNS (bool)', description,
					lambda x: True if x == 0 or x == 1 else False )

class Module( framework.Module ):
	def init( self ):
		check = self.validate_options()
		if( not check ):
			self.error( 'Option %s not set.' % ( check ) )
			return
		self.success( 'Started' )
		self.db = framework.db()
		if( self.option.domain is None ):
			if( self.option.re_resolve ):
				self.domains = self.db.getDomainNames()
			else:
				self.domains = self.db.getDomainNames( resolved = None )
		else:
			self.db.domain( self.option.domain )
			self.domains = [ self.option.domain, ]
		self.resolver = dns.Resolver( self.option.standard_dns, self.option.ns )
		self.resolver.timeout = self.option.ns_timeout
		socket.setdefaulttimeout( self.option.socket_timeout )
		self.dnslookup()
	def stop( self ):
		self.tl.hardstop()
	def pause( self ):
		self.tl.pause()
	def resume( self ):
		self.tl.resume()
	def dnslookup( self ):
		self.tl = thread.Launcher( 	dnslookup,
									self.option.threads,
									args = ( self, ),
									put_limit = 1 )
		input = False
		for domain in self.domains:
			input = True
			if( not self.active() ):
				self.exit()
			self.tl.put( domain )
		self.tl.stop()
		self.tl.wait()
		if( not input ):
			self.warn( 'No input' )
		self.success( 'Finished' )

def dnslookup( thread, self ):
	while( thread.active() ):
		domain = thread.input()
		try:
			ips = self.resolver.query( domain )
			for ip in ips:
				self.info( 'Domain %s resolves to %s' % ( domain, ip.address ) )
				self.db.setIP( ip.address )
				self.db.mapHosts( ip.address, domain )
		except:
			#import traceback; traceback.print_exc()
			pass
		finally:
			self.db.updateDomain( domain, resolved = True )
			time.sleep( self.option.ns_cooldown )
