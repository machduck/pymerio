#!/usr/bin/python

import framework
# everything that we need is in framework
from libs import thread
# import library for thread management
from libs import web
# import library for web management

description = '''This module checks for robots.txt and submits results
	to database.'''
# one tab for pretty formatting

framework.info( name = 'robots',
				author = framework.__author__,
				description =  description )
# comments are optional for info()

description = '''Default argument takes as input all domains in db.
	Ex: http://site.tld/'''
# one tab for pretty formatting
framework.option( 'url', None, 'Url', description )
# register option, second arg is True if option required, otherwise default val

description = '''Default amount of threads for fetching robots.txt files'''
framework.option( 'threads', 0x5, 'Threads', description )
# register option

framework.option( 'socket_timeout', 30, 'Socket timeout' )
framework.option( 'http_timeout', 30, 'HTTP timeout' )
framework.option( 	're_check',
					0x0,
					'Re-check robots.txt (bool)',
					validate = lambda x: True if x == 0 or x == 1 else False )

import urllib2
import socket
import re
from urlparse import urlparse
# import module specific modules

class Module( framework.Module ):
	# main class name must be Module
	# parent framework.Module allows multithread modules
	def init( self ):
		# main code goes here
		check = self.validate_options()
		# validate options
		if( not check ):
			# if validation failed
			self.error( 'Option %s not set.' % ( check ) )
			# log error
			return
			# exit module
		self.success( 'Module initialized' )
		# log that we're good to go
		socket.setdefaulttimeout( self.option.socket_timeout )
		# set default socket timeout
		self.db = framework.db() 
		# get database object
		robots = 'robots.txt'

		input = False # generators dont know their length, manual check
		if( self.option.url is None ):
			self.tl = thread.Launcher( 	mt_robots, # target function
										self.option.threads, # thread num
										args=( self, ), # args to mt_robots
										put_limit = 1 ) # queue size
			# create threads
			if( self.option.re_check ):
				domains = self.db.getDomainNames()
			else:
				domains = self.db.getDomainNames( robots = None )
			# get domains from database object
			for domain in domains: # generator in use
				input = True
				if( not self.active() ):
					# can be interrupted here
					self.exit() 
					# abort thread
				ports = False # to access domains we need to know their ports

				url =  'http://' + domain + '/' + robots
				# create url
				self.tl.put( url )
				# add url to target queue
				url = 'https://' + domain + '/' + robots
				self.tl.put( url )
				# repeat
			self.tl.stop()
			# send the stop signals to queue
			self.tl.wait()
			# wait for ack rst from queue
		else:
			# use url from option 'url'
			input = True
			url = self.option.url.strip('/') + '/robots.txt'
			self.info( 'Checking url: %s' % ( url ) )
			# log which domain / file we're checking for
			# view with output command
			req_robots( self, url )
			# call request function
		if( not input ):
			self.warn( 'No input' )
		self.success( 'Module exiting.' )
		# mop up here, say goodbye
	def stop( self ):
		# explicitely stopped
		if( self.option.url is None ):
			self.tl.hardstop()
	def pause( self ):
		# pause hook, prepare for the pausing of module
		if( self.option.url is None ):
			self.tl.pause()
	def resume( self ):
		# resume hook, prepare for the resuming of module
		if( self.option.url is None ):
			self.tl.resume()
	def valid_html( self, html ):
		# not actually valid, just not a robots.txt file
		rvalid_html = '[^#]*<html.*?>.*' # good enough, if bugs, look here
		r = re.match( rvalid_html, html, re.MULTILINE and re.IGNORECASE )
		if( r ):
			return True
		else:
			return False

def mt_robots( thread, self ):
	while( thread.active() ):
		# if thread has stuff to do
		url = thread.input()
		# get input from queue
		req_robots( self, url )

def req_robots( self, url ):
	self.info( 'Checking url: %s' % ( url ) )
	# log which domain we're checking for
	# view with output command
	# thread safe
	base = url.strip( '/robots.txt' )
	# save url basename for later use
	disallow = re.escape( 'Disallow:' )
	allow = re.escape( 'Allow:' )
	sitemap = re.escape( 'Sitemap:' )
	ua = re.escape( 'User-agent:' )
	# set some constants for robots.txt file parsing
	try:
		opener = web.socks_opener()
		# use custom socks opener
		r = opener.open( url, timeout = self.option.http_timeout )
		# make request
		r = r.read()
		# read data
	except urllib2.HTTPError, e:
		# http error retrieving page - log, return
		self.warn( '%s for url: %s' % ( e.getcode(), url ) )
		return
	except socket.timeout, e:
		# timeout
		self.warn( 'Timeout for %s' % ( url ) )
		return
	except ValueError:
		# invalid url supplied
		self.error( 'Invalid url supplied: %s' % ( url ) )
		return
	except: # BadStatusLine
		self.error( 'Some strange error happened in urllib2: %s' % ( url ) )
		return
	domain = urlparse( url ).hostname
	self.db.updateDomain( domain, robots = True )
	if( self.valid_html( r ) ):
		# if valid html returned - page exists, add page, log error, return
		self.warn( '200 OK html for url: %s' % ( url ) )
		self.db.setURL( url, hash = framework.hash( r ) )
		return
	start = '^'
	capture_group = '\s*([^\s]*)'
	flags = re.MULTILINE
	rdisallow = start+disallow+capture_group
	rallow = start+allow+capture_group
	rsitemap = start+sitemap+capture_group
	rua = start+ua+capture_group
	# create regexes
	md = re.findall( rdisallow, r , flags )
	ma = re.findall( rallow, r , flags )
	ms = re.findall( rsitemap, r , flags )
	mu = re.findall( rua, r , flags )
	matches = md + ma + ms
	# run regexes, .group(0) holds all matches
	for m in matches:
		if( m.startswith( '/' ) ): # short url: /..
			url = base + m
		else: # normal url http://..
			url = m
		self.db.setURL( url ) # thread safe
		self.info( 'Url found: %s' % ( url ) ) # as is this
		# populate database with elements and log
