#!/usr/bin/python

import sys
sys.path.append( './core/' )
sys.path.append( './lib/' ) 
from pymerio import Pymerio
from shell import Shell
#from shell import Interpretor

def main():
	try:
		pymerio = Pymerio()
		#pymerio.quiet_load = 1
		#pymerio.color( False )
		pymerio.load_modules()
		Shell( pymerio ).cmdloop()
		#Interpretor( pymerio ).cmdloop()
		#Shell( pymerio ).onecmd( 'help' )
	except KeyboardInterrupt:
		print ''
		print ''
		print ' Exiting..'
		print ''
		sys.exit()

if __name__ == "__main__" :
	main()
