#!/usr/bin/python

import sys
import threading
import db

class OutputColor():
	Y=N=R=O=IG=B=''
	def color( self, bool ):
		if( bool ):
			self.N = '\033[m'
			self.R = '\033[31m'
			self.O = '\033[33m'
			self.IG = '\033[92m'
			self.B = '\033[34m'
			self.Y = '\033[93m'
		else:
			self.Y=self.B=self.N=self.R=self.O=self.IG=''

class Output( OutputColor ):
	'''Output functions'''
	def out( self, message ): # specific to this class
		print message
	def outunb( self, message ): # specific to this class
		sys.stdout.write( message )
		sys.stdout.flush()
	def error( self, message, module = None ):
		if( module is None ):
			print( ' %s[-]%s %s%s' % ( self.R, self.O, message, self.N ) )
		else:
			print( ' %s[-]%s [ %s ]%s %s%s' % ( self.R,
												self.B, 
												module, 
												self.O, 
												message, 
												self.N ))
	def warn( self, message, module = None ):
		if( module is None ):
			print( ' %s[!]%s %s%s' % ( self.R, self.O, message, self.N ) )
		else:
			print( ' %s[!]%s [ %s ]%s %s%s' % ( self.R,
												self.B, 
												module, 
												self.O, 
												message, 
												self.N ))

	def info( self, message, module = None ):
		if( module is None ):
			print( ' %s[*]%s %s%s' % ( self.B, self.O, message, self.N ) )
		else:
			print( ' %s[*] [ %s ]%s %s%s' % ( 	self.B,
												module, 
												self.O, 
												message, 
												self.N ))
	def success( self, message, module = None ):
		if( module is None ):
			print( ' %s[+]%s %s%s' % ( self.IG, self.O, message, self.N ) )
		else:
			print( ' %s[+]%s [ %s ]%s %s%s' % ( self.IG,
												self.B, 
												module, 
												self.O, 
												message, 
												self.N ))
	def unknown( self, message, module = None ): # specific to this class
		if( module is None ):
			print( ' %s[?]%s %s%s' % ( self.R, self.O, message, self.N ) )
		else:
			print( ' %s[?]%s [ %s ]%s %s%s' % ( self.R,
												self.B,
												module,
												self.O,
												message,
												self.N ))
	def trace( self, message, module = None ):
		if( module is None ):
			print( '%s%s%s' % ( self.N + '-'*80 + self.R + '\n\n',
								message,
								'\n' + self.N + '-'*80 ) )
		else:
			print( ' %s[-]%s [ %s ]%s Module failed!%s' % ( self.R, 
															self.B,
															module,
															self.O,
															self.N ) )
			print( '%s%s%s' % (	self.N + '-'*80 + self.R + '\n\n',
								message,
								'\n' + self.N + '-'*80 ) )

class OutputModule( OutputColor ):
	_output_add = threading.Event()
	_output_fin = threading.Event()
	_monitor = threading.Event()
	_olock = threading.Lock()
	_modified = ''
	_mid = ''
	db = db.SingletonDB.getInstance()
	def __init__( self, name ):
		self.name = name
	def info( self, message ):
		'''Inform user of something happening'''
		mid = OutputModule.db.setInfo( message = message, module = self.name )
		self._msg( message, mid )
	def error( self, message ):
		'''An error effecting the logic of a module happened'''
		mid = OutputModule.db.setError( message = message, module = self.name )
		self._msg( message, mid )
	def trace( self, message ):
		'''Debugging purposes: traceback.format_exc()'''
		mid = OutputModule.db.setTrace( message = message, module = self.name )
		self._msg( message, mid )
	def warn( self, message ):
		'''Module recieved some bad results but was able to recover'''
		mid = OutputModule.db.setWarn( message = message, module = self.name )
		self._msg( message, mid )
	def success( self, message ):
		'''Module started up / shutdown successfully'''
		mid = OutputModule.db.setSuccess( message = message, module = self.name )
		self._msg( message, mid )
	def _msg( self, message, mid ):
		if( OutputModule._monitor.isSet() ): # watch
			OutputModule._olock.acquire() # writes don't lock! :]
			OutputModule._modified = self.name
			OutputModule._mid = mid
			OutputModule._output_add.set()
			OutputModule._output_fin.wait()
			OutputModule._output_fin.clear()
			OutputModule._output_add.clear()
			OutputModule._olock.release()
