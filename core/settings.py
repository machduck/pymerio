#!/usr/bin/python

class Settings():
	opts = {}
	infos = {}
	mod = ''
	@staticmethod
	def set_opt( name, default, description, comment, validate, required ):
		if( Settings.mod not in Settings.opts.keys() ):
			Settings.opts[ Settings.mod ] = {}
		Settings.opts[ Settings.mod ][ name ] = {	'name': name,
													'default': default,
													'description': description,
													'comment': comment,
													'validate': validate,
													'required': required }
	@staticmethod
	def set_info( name, author, description, comment ):
		Settings.infos[ Settings.mod ] = {	'name': name,
											'author': author,
											'description': description,
											'comment': comment }
	@staticmethod
	def set_mod( module ):
		Settings.mod = module

class Extract( dict ): # inherit from dict to iterate via .keys()
	def __init__( self, dct ):
		dict.__init__( self, dct ) # instantiate dict upon thyself
		self.__dict__.update( dct )
