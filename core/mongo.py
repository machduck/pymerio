#!/usr/bin/python

import pymongo
import datetime
import os
import ConfigParser
import re
import urlparse
from bson.dbref import DBRef
from functools import wraps

def num( f ):
	@wraps(f)
	def wrapper( *args, **kwargs ):
		return f( *args, **kwargs )['n'] # return number of objects
	return wrapper
def name( f ):
	@wraps( f )
	def wrapper( *args, **kwargs ):
		return f( *args, **kwargs )['name'] # return name field
	return wrapper

@name
def getName( x ):
	return x
@num
def getNum( x ):
	return x

def message( f ):
	@wraps( f )
	def wrapper( *args, **kwargs ):
		return f( *args, **kwargs )['message'] # return message field
	return wrapper
def format( f ):
	@wraps( f )
	def wrapper( *args, **kwargs ):
		return f( *args, **kwargs )['format'] # return format field
	return wrapper
def module( f ):
	@wraps( f )
	def wrapper( *args, **kwargs ):
		return f( *args, **kwargs )['module'] # return module field
	return wrapper

@message
def getMessage( log ):
	return log
@format
def getFormat( log ):
	return log
@module
def getModule( log ):
	return log

def boolean( f ):
	@wraps( f )
	def wrapper( *args, **kwargs ):
		return bool( f( *args, **kwargs ) ) # return bool
	return wrapper

class AlterMongoDB(): # TODO: add alternate api ?
	def set_session( self, *args, **kwargs ):
		return self.setSession( *args, **kwargs )
	def del_session( self, *args, **kwargs ):
		return self.delSession( *args, **kwargs )
	def get_sessions( self, *args, **kwargs ):
		return self.getSessions( *args, **kwargs )

	def set_domain( self, *args, **kwargs ):
		return self.setDomain( *args, **kwargs )
	def del_domain( self, *args, **kwargs ):
		return self.delDomain( *args, **kwargs )
	def get_domains( self, *args, **kwargs ):
		return self.getDomains( *args, **kwargs )

	def set_ip( self, *args, **kwargs ):
		return self.setIP( *args, **kwargs )
	def del_ip( self, *args, **kwargs ):
		return self.delIP( *args, **kwargs )
	def get_ips( self, *args, **kwargs ):
		return self.getIPs( *args, **kwargs )
	def insert_range( self, *args, **kwargs ):
		return self.insertIPRange( *args, **kwargs )

	def get_session_names( self, *args, **kwargs ):
		return self.getSessionNames( *args, **kwargs )
	def clone_session( self, *args, **kwargs ):
		return self.cloneSession( self, *args, **kwargs )

	def deref( self, *args, **kwargs ):
		self.dereference( *args, **kwargs )

class LegacyAPI():
	def scan( self, *args, **kwargs ):
		return self.setSession( *args, **kwargs )
	def ip( self, *args, **kwargs ):
		return self.setIP( *args, **kwargs )
	def domain( self, *args, **kwargs ):
		return self.setDomain( *args, **kwargs )
	def delete( self, *args, **kwargs ):
		return self.delSession( *args, **kwargs )

class MongoDB( LegacyAPI, AlterMongoDB ):
	def __init__( self ):
		self._session = None
		file = os.path.join(os.path.abspath(os.path.dirname(__file__)),
							'..',
							'settings.conf' )
		config = ConfigParser.ConfigParser()
		config.read( file )
		self._host = config.get( 'mongodb', 'host' )
		self._port = int( config.get( 'mongodb', 'port' ) )
		self._dbname = config.get( 'mongodb', 'database' )
		self._pass = config.get( 'mongodb', 'pass' )
		self._user = config.get( 'mongodb', 'user' )
		self._auth = config.get( 'mongodb', 'auth' ).upper()
		if( self._auth == 'TRUE' ):
			self._auth = True
		else:
			self._auth = False
		self._client = pymongo.MongoClient( self._host, self._port )
		self._db = self._client[ self._dbname ]
		if( self._auth ):
			try:
				self._db.authenticate( self._user, self._pass )
			except:
				raise Exception( 'Database authentication failed!' )
		self._session_c = config.get( 'mongodb', 'session' )
		self._domain_c = config.get( 'mongodb', 'domain' )
		self._ip_c = config.get( 'mongodb', 'ip' )
		self._node_c = config.get( 'mongodb', 'node' )
		self._data_c = config.get( 'mongodb', 'data' )
		self._log_c = config.get( 'mongodb', 'log' )
		self._session_sub_c = [ self._domain_c,
								self._ip_c, 
								self._node_c, 
								self._data_c,
								self._log_c ]

	def install( self ):
		pass
	def createIndexes( self ):
		# create indexes, avoid duplicates
		# called from install(), setSession()
		#db.collection.ensureIndex({ 'val':1 },{ unique: true, dropDups: true })
		self._db[self._session][ self._ip_c ].ensure_index(	[('ip', 1)],
															unique = True,
															dropDups = True)
		# TODO: create other indexes with ensueIndex

	def findSession( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._db[ self._session_c ].find_one( kwargs )

	def findIP( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._ip_c ].find_one( kwargs )

	def findDomain( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._domain_c ].find_one( kwargs )

	def setSession( self, name, dict = {}, **kwargs ): # upsert ?
		# return true if sessions set
		# return false if session exists
		# sets self._db  -- db.pymerio context
		# sets self._dbc -- db.pymerio.session context
		self._session = name
		self._db = self._client[ self._dbname ]
		session_key = { 'name': name }
		insert = { 'name': name, 'date': datetime.datetime.utcnow() }
		insert.update( kwargs ) # overwrite any standard values
		insert.update( dict ) # overwrite any values

		session_doc = self.findSession( session_key )
		if( session_doc is None ):
			self.createSession( insert )
			rflag = True
		else:
			if( len( kwargs.keys() ) > 0 ):
				kwargs.update( dict )
				self.updateSession( name, kwargs )
			rflag = False

		self._dbc = self._client[ self._dbname ][ name ]
		self.createIndexes()

		return rflag

	def setIP( self, ip, dict = {}, **kwargs ): # upsert ?
		# return true if ip added
		# return false if ip existed
		key = { 'ip': ip }
		insert = { 'ip': ip, #}
					'domains': [],
					'reverseip': None, 
					'ptr': None,
					'scanned': None }
		insert.update( kwargs )
		insert.update( dict )

		ip_doc = self.findIP( key )
		if( ip_doc is None ):
			self.createIP( insert )
			return False
		else:
			if( len( kwargs.keys() ) > 0 ):
				kwargs.update( dict )
				self.updateIP( ip, kwargs )
			return True
	
	def setDomain( self, domain, dict = {}, **kwargs ): # upsert ?
		# return true if domain added
		# return false if domain existed
		key = { 'domain': domain }
		insert = { 	'domain': domain, #}
					'ips': [],
					'spidered': None,
					'resolved': None,
					'bruteforced': None,
					'robots': None,
					'subdomain': None,
					'subnets': None,
					'tic': None,
					'pr': None }
		insert.update( kwargs )
		insert.update( dict )

		domain_doc = self.findDomain( key )
		if( domain_doc is None ):
			self.createDomain( insert )
			return False
		else:
			if( len( kwargs.keys() ) > 0 ):
				kwargs.update( dict )
				self.updateDomain( domain, kwargs )
			return True

	@num
	def delDomain( self, dict = {}, **kwargs ):
		# return true if domain existed
		# return false if domain didn't exist
		kwargs.update( dict )
		return self._dbc[ self._domain_c ].remove( kwargs )
	@num
	def delIP( self, dict = {}, **kwargs ):
		# return true if ip existed
		# return false if ip didn't exist
		kwargs.update( dict )
		return self._dbc[ self._ip_c ].remove( kwargs )

	def insertIPRange( self, ip_range ):
		# add ip ranges to database
		# format: 192-193.168-170.0-255.1-24
		# format: 192.168.0.1-255
		# return nothing -- too much objects
		r = re.match(  '^([0-9]{1,3})(?:-([0-9]{1,3}))?\.'
						'([0-9]{1,3})(?:-([0-9]{1,3}))?\.'
						'([0-9]{1,3})(?:-([0-9]{1,3}))?\.'
						'([0-9]{1,3})(?:-([0-9]{1,3}))?$',
						ip_range )
		range =  zip( r.groups()[0::2], r.groups()[1::2] )
		range = [ (int(c),int(d)) for (c,d) in [
						(lambda a,b: (a,b) if b is not None else (a,a))(x,y)
						for (x,y) in range ]
				]
		i = 0
		nagle = 1000 # badass hardcoding
		buffer = []
		for ip_address in self.gen_range( range ):
			d = { 'ip': ip_address }
			buffer.append( d )
			i = i + 1
			if( i % nagle == 0 ):
				self.safeBulkInsertIP( buffer )
				buffer = []
		if( i % nagle > 0 ):
			self.safeBulkInsertIP( buffer )

	def gen_range( self, ip ):
		# ip range generator
		# ip format: [ (a,a) (b,b), (c,c), (d,d) ]
		for a in range( ip[0][0], ip[0][1] + 1 ):
			for b in range( ip[1][0], ip[1][1] + 1 ):
				for c in range( ip[2][0], ip[2][1] + 1 ):
					for d in range( ip[3][0], ip[3][1] + 1 ):
						 yield '%d.%d.%d.%d' % ( a, b, c, d )

	def bulkInsertIP( self, iterable ):
		return self._dbc[ self._ip_c ].insert( iterable )

	def safeBulkInsertIP( self, iterable ):
		try:
			self.bulkInsertIP( iterable )
		except:
			for item in iterable:
				try:
					self._dbc[ self._ip_c ].insert( item )
				except:
					pass

	def getIPs( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._ip_c ].find( kwargs )

	def getDomains( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._domain_c ].find( kwargs )

	def getDomainNames( self, dict = {}, **kwargs ): # TODO: map reduce
		#domain_docs = self.getDomains( *args, **kwargs )
		kwargs.update( dict )
		domain_docs = self._dbc[ self._domain_c ].find( kwargs )
		for domain_doc in domain_docs:
			try:
				yield( domain_doc['domain'] )
			except:
				pass

	def getIPAddresses( self, dict = {}, **kwargs ): # TODO: map reduce
		#ip_docs = self.getIPs( *args, **kwargs )
		kwargs.update( dict )
		ip_docs = self._dbc[ self._ip_c ].find( kwargs )
		for ip_doc in ip_docs:
			try:
				yield( ip_doc['ip'] )
			except:
				pass

	def getDomainNamesByIP( self, *args, **kwargs ): # TODO: map reduce
		for domaindoc in self.getDomainsByIP( *args, **kwargs ):
			try:
				yield( domaindoc['domain'] )
			except:
				pass
	def getIPAddressesByDomain( self, *args, **kwargs ): # TODO: map reduce
		for ipdoc in self.getIPsByDomain( *args, **kwargs ):
			try:
				yield( ipdoc['ip'] )
			except:
				pass

	def getDomainsByIP( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		#domains = []
		ipdocs = self._dbc[ self._ip_c ].find( kwargs )
		for ipdoc in ipdocs:
			try:
				for domainref in ipdoc['domains']:
					domaindoc = self.dereference(domainref)
					#domains.append( domaindoc['domain'] )
					yield( domaindoc )
			except:
				pass
		#return domains

	def getIPsByDomain( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		#ips = []
		domaindocs = self._dbc[ self._domain_c ].find( kwargs )
		for domaindoc in domaindocs:
			try:
				for ipref in domaindoc['ips']:
					ipdoc = self.dereference(ipref)
					#ips.append( ipdoc['ip'] )
					yield( ipdoc )
			except:
				pass
		#return ips

	def getSessions( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._db[ self._session_c ].find( kwargs )


	def delSession( self, session ):
		# return true if every column deleted
		# return false if one or more column did not exist
		key = { 'name': session }
		r = bool(getNum(self._db[self._session_c].remove( key )))
		for sub_c in self._session_sub_c:
			r &= bool( self._db[ session ][ sub_c ].drop() )
		return r

	def getSessionNames( self, *args, **kwargs ):
		return [ getName(x) for x in self.getSessions( *args, **kwargs ) ]

	def cloneSession( self, source, destination ):
		# clone source session
		# return true if existed
		# return false if created
		session_doc = self.findSession({ 'name': destination })
		if( session_doc is not None ):
			raise Exception( 'Session exists!' ) #return True

		insert = { 	'name': destination,
					'date': datetime.datetime.utcnow() }
		self.createSession( insert )

		for sub_c in self._session_sub_c:
			if( self._db[ source ][ sub_c ].count() > 0 ):
				self._db[ destination ][ sub_c ].insert( 
						self._db[ source ][ sub_c ].find()
				)
		return False

	def setLog( self, 
				message, 
				dict={}, 
				module=None, 
				type=None, 
				format=None, 
				**kwargs ):
		# insert CUSTOM message
		insert = { 	'type':    type,
					'date':    datetime.datetime.utcnow(),
					'module':  module,
					'message': message,
					'format': format }
		insert.update( kwargs )
		insert.update( dict )
		return self._dbc[ self._log_c ].insert( insert )
	def setInfo( self, *args, **kwargs ):
		# insert INFO message
		# kwargs: message, module
		return self.setLog( *args, type = 'info', format = 'info', **kwargs )
	def setError( self, *args, **kwargs ):
		# insert ERROR message
		# kwargs: message, module
		return self.setLog( *args, type = 'error', format = 'error', **kwargs )
	def setTrace( self, *args, **kwargs ):
		# insert TRACE message
		# kwargs: message, module
		return self.setLog( *args, type = 'error', format = 'trace', **kwargs )
	def setWarn( self, *args, **kwargs ):
		# insert WARN message
		# kwargs: message, module
		return self.setLog( *args, type = 'error', format = 'warn', **kwargs )
	def setSuccess( self, *args, **kwargs ):
		# insert SUCCESS message
		# kwargs: message, module
		return self.setLog( *args, type = 'info', format = 'success', **kwargs )

	def getLogs( self, dict={}, size = 38, offset = 0, all = False, **kwargs ):
		# kwargs: module, type
		kwargs.update( dict )
		if( all == True ):
			return self._dbc[ self._log_c ].find( kwargs ).sort('_id',1)
		else:
			cursor = self._dbc[ self._log_c ].find( kwargs ).sort('_id',1)
			cnt = cursor.count()
			size = int( size )
			offset = int( offset )
			skip = cnt - size - offset
			if( size + offset > cnt ):
				size = cnt - offset
			if( offset >= cnt ):
				skip = cnt
			if( skip < 0 ):
				skip = 0
			cursor.skip( skip ).limit( size )
			return cursor
	def getInfo( self, *args, **kwargs ):
		return self.getLogs( *args, type = 'info', **kwargs )
	def getErrors( self, *args, **kwargs ):
		return self.getLogs( *args, type = 'error', **kwargs )

	def getLastLog( self, dict = {}, **kwargs ): 
		# kwargs: module, type
		# called db.getLastLog( module = 'module', type = 'type' )
		kwargs.update( dict )
		return self._dbc[self._log_c].find(kwargs).sort('_id',-1).limit(1)[0]

	def delLogs( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._log_c ].remove( kwargs )
	@num
	def delInfo( self, *args, **kwargs ):
		return self.delLogs( *args, type = 'info', **kwargs )
	@num
	def delErrors( self, *args, **kwargs ):
		return self.delLogs( *args, type = 'error', dict = dict, **kwargs )

	def dereference( self, ref ):
		# dereference DBRef object
		return self._db.dereference( ref )

	def mapHosts( self, ip, domain ):
		r = self.mapIPToDomain( ip, domain )
		r &= self.mapDomainToIP( domain, ip )
		return r

	@boolean
	def mapIPToDomain( self, ip, domain ):
		# associate ip with domain
		# return true if of
		# return false if domain or ip not found
		domain = self.findDomain({'domain': domain })
		if( domain is None ):
			return False
		return self._dbc[ self._ip_c ].update( 
				{ 'ip': ip },
				{ '$addToSet': { 'domains': DBRef( 	self._session + 
															'.' + 
															self._domain_c, 
													domain['_id'] ) 
		}})

	@boolean
	def mapDomainToIP( self, domain, ip ):
		# associate domain with ip
		# return true if ok
		# return false if ip or domain not found
		ip = self.findIP({ 'ip': ip })
		if( ip is None ):
			return False
		return self._dbc[ self._domain_c ].update(
				{ 'domain': domain },
				{ '$addToSet': { 'ips': DBRef( 	self._session + 
														'.' +
														self._ip_c,
												ip['_id'] )
		}})

	def updateDomain( self, domain, dict={}, action = '$set', **kwargs ):
		# update `kwargs`dict keys fields of `domain` via `action`
		kwargs.update( dict )
		return self.extUpdateDomain(
			{ 'domain': domain },
			{ action: kwargs },
		)
	def updateIP( self, ip, dict={}, action = '$set', **kwargs ):
		# update `kwargs`dict keys fields of `ip` via `action`
		kwargs.update( dict )
		return self.extUpdateIP(
			{ 'ip': ip },
			{ action: kwargs },
		)
	def updateSession( self, session, dict={}, action = '$set', **kwargs ):
		# update kwargs dict keys fields of `session` via  `action`
		kwargs.update( dict )
		return self.extUpdateSession(
			{ 'session': session },
			{ action: kwargs },
		)

	def extUpdateIP( self, ip, update, upsert = False ):
		# update `update` dict keys fields in ip doc found via `ip` dict
		# example:
		#     ip = { 'ip': 'ip' }
		#     update = { '$set' : { 'field': 'key' } }
		return self._dbc[ self._ip_c ].update( ip, update, upsert )

	def extUpdateDomain( self, domain, update, upsert = False ):
		# update `update` dict keys fields in domain doc found via `domain` dict
		# example:
		#     domain = { 'domain': 'domain' }
		#     update = { '$set' : { 'field': 'key' } }
		return self._dbc[ self._domain_c ].update( domain, update, upsert )

	def extUpdateSession( self, session, update, upsert = False ):
		# update `update` dict keys fields in session doc found via `session`
		# dict
		# example:
		#     session = { 'session': 'session' }
		#     update = { '$set' : { 'field': 'key' } }
		return self._db[ self._session_c ].update( session, update, upsert )

	# 
	# Alpha
	#

	def extUpdateNode( self, node, update, upsert = False ):
		return self._dbc[ self._node_c ].update( node, update, upsert )

	def extUpdateData( self, data, update, upsert = False ):
		return self._dbc[ self._data_c ].update( data, update, upsert )

	@num
	def delNode( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._node_c ].remove( kwargs )

	@num
	def delData( self, dict = {}, **kwargs ):
		return self._dbc[ self._node_c ].remove( kwargs )

	def setURL( self,
				url,
				post = {}, # post dictionary
				xheaders = {}, # extra auth headers
				node = {}, # additional node values
				request = {}, # full request headers
				response = {}, # full response headers
				html = None, # html recieved
				hash = None, # hash, usually framework.hash( html )
				data = {}, # additional data values
				add_data = False ):
		purl = urlparse.urlparse( url )
		
		netloc = purl.netloc
		scheme = purl.scheme
		domain = purl.hostname

		self.setDomain( domain )

		rpath = purl.path
		query = purl.query
		frag = purl.fragment

		get_dict = dict([ y for y in 
				[ (lambda a: a if len(a)==2 else [a[0],''])(x.split('=')) 
					for x in query.split('&') ] 
				if( len( y ) > 0 and y[0] != '' ) ])
		get = get_dict.keys()

		path = filter( None, rpath.split('/') )

		post_dict = post
		post = post_dict.keys()

		xtraheaders_dict = xheaders
		xtraheaders = xheaders.keys()

		node_key = {'get': get,
					'post': post,
					'scheme': scheme,
					'netloc': netloc,
					'domain': domain,
					'path': path,
					'xtraheaders': xtraheaders }
		node_data = { 	'data': [],
						'dynamic': None }
		insert = {}
		insert.update( node_key )
		insert.update( node_data )
		insert.update( node )

		node_doc = self.findNode( node_key )
		if( node_doc == None ):
			node_id = self.createNode( insert )
			node_existed = False
		else:
			node_existed = True
			node_id = node_doc['_id']

		if( add_data == False and node_existed == True ):
			return node_existed

		data_key = { 	'parent_id': node_id,
						'get': get_dict,
						'post': post_dict,
						'xtraheaders': xtraheaders_dict }
		data_data = { 	'html': html,
						'hash': hash,
						'request': request,
						'response': response }
		insert = {}
		insert.update( data_key )
		insert.update( data_data )
		insert.update( data )

		data_doc = self.findData( data_key )
		if( data_doc is None ):
			self.createData( insert )

		return node_existed

	def getNodes( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._node_c ].find( kwargs )

	def getData( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._data_c ].find( kwargs )

	def findNode( self, dict = {}, **kwargs ):
		kwargs.update( dict  )
		return self._dbc[ self._node_c ].find_one( kwargs )

	def findData( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		return self._dbc[ self._data_c ].find_one( kwargs )

	def createSession( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		if( len( kwargs.keys() ) == 0 ): return None
		return self._db[ self._session_c ].insert( kwargs )

	def createIP( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		if( len( kwargs.keys() ) == 0 ): return None
		return self._dbc[ self._ip_c ].insert( kwargs )

	def createDomain( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		if( len( kwargs.keys() ) == 0 ): return None
		return self._dbc[ self._domain_c ].insert( kwargs )

	def createNode( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		if( len( kwargs.keys() ) == 0 ): return None
		return self._dbc[ self._node_c ].insert( kwargs )

	def createData( self, dict = {}, **kwargs ):
		kwargs.update( dict )
		if( len( kwargs.keys() ) == 0 ): return None
		return self._dbc[ self._data_c ].insert( kwargs )

	#def setDomainPort( self, domain, port ):
		#self.updateDomain( domain, '$addToSet', {'ports': port} )
	#def getDomainPortNumbers( self, domain ):
		#domain_doc = self.findDomain({'domain': domain })
		#try:
			#for port in domain_doc['ports']:
				#yield( port )
		#except:
			#pass
	#def getDomainPorts( self, *args, **kwargs ):
		#for port in self.getDomainPortNumbers( *args, **kwargs ):
			#port = str( port )
			#if( port == '80' ):
				#yield ''
			#else:
				#yield( ':' + port )

#
# MongoDB operators
#
# push - push into array
# addToSet - push if not in set
# set - overwrite value
# upsert - create if not exists
