#!/usr/bin/python
# -*- coding: utf-8

import cmd
import readline # dirty hack for autocompletion
import os
import sys
from subprocess import call
import output
import shlex
import re
import settings
import db
import convenience
import time
import mongo
import threading

class ShellBase( cmd.Cmd, output.Output, convenience.Convenience ):
	def __init__( self ):
		self.module = ''
		self.prompt = self._prompt % ( self.pymerio.g_opts['session'], '' )
		self.base_path = os.getcwd()
		self.doc_header = 'Commands (type [help|?] <topic>):'
		self.undoc_header = ''
		self.misc_header = ''
		self.ruler = '-'
		self.nohelp = ' %s[-]%s No help for command: %%s%s' % ( self.R, 
																self.O, 
																self.N )
		self.identchars = self.identchars + ':'
		# start dirty hack
		delims=readline.get_completer_delims()\
											.replace(':','')\
											.replace('/','')\
											.replace('-','')
		# O_O
		# oh noes! te horror!
		readline.set_completer_delims(delims)
		# end dirty hack, jk, this whole project is a big dirty hack
		cmd.Cmd.__init__( self )
		self.db = db.SingletonDB.getInstance()

	def onecmd( self, line ): # failsafe
		try:
			return cmd.Cmd.onecmd( self, line )
		except:
			import traceback; traceback.print_exc()
			self.error( 'An unknown error occurred. Check your input.' )

	def do_EOF( self, line ): # hidden from help
		if( self.module != '' ):
			self.module = ''
			self.prompt = self._prompt % ( self.pymerio.g_opts['session'], '' )
			self.out( '' )
			return False
		self.out( '' )
		return True

	def emptyline( self ):
		pass # do nothing on empty lines
	def default( self, line ): # bash command
		self.do_shell( line )
	def do_shell( self, line ): # python command
		'''Execute shell command'''
		self.info('Command: %s' % ( line ) )
		self.outunb( self.O )
		try:
			call( line , shell=True)
		except:
			self.error('Error executing command: %s' % ( line ) )
		self.outunb( self.N )
	def do_py( self, line ):
		'''Execute python command'''
		try:
			exec( line ) 
		except Exception, e:
			self.out( '%s%s:%s%s' % ( self.O, e.__class__,e, self.N ) )

	def completedefault( self, text, line, begidx, endidx ):
		pass # do nothing if no complete_* function specified for input

	def do_use( self, module ):
		'''Select module'''
		if( module not in self.pymerio.modules.keys() ):
			self.error( 'Module %s not loaded.' % ( module ) )
			return
		self.module = module
		self.prompt = self._prompt % ( 	self.pymerio.g_opts['session'],
										' ' + module + ' ' )

	def do_cd( self, path ): # emulate cd behavior, hidden from help
		if( path.strip() != '' ):
			try:
				os.chdir( path ) 
			except:
				self.error( "Directory %s doesn't exist." % ( path ) )
		else: 
			try:
				os.chdir( self.base_path )
			except:
				self.error( "Directory %s stopped existing." % ( path ) )

	def do_run( self, line ):
		'''Run module'''
		if( line.strip() == '' and self.module != '' ):
			self.pymerio.run_module( self.module )
		elif( line.strip() == '' ):
			self.error( 'No module selected' )
		else:
			modules = shlex.split( line )
			for module in modules:
				self.pymerio.run_module( module )
	def do_stop( self, line ):
		'''Stop module'''
		if( line.strip() == '' and self.module != '' ):
			self.pymerio.stop_module( self.module )
		elif( line.strip() == '' ):
			self.error( 'No module selected' )
		else:
			if( line.strip() == 'all' ):
				modules = self.pymerio.active.keys()
			else:
				modules = shlex.split( line )
			for module in modules:
				self.pymerio.stop_module( module )
	def do_pause( self, line ):
		'''Pause module'''
		if( line.strip() == '' and self.module != '' ):
			self.pymerio.pause_module( self.module )
		elif( line.strip() == '' ):
			self.error( 'No module selected' )
		else:
			if( line.strip() == 'all' ):
				modules = self.pymerio.active.keys()
			else:
				modules = shlex.split( line )
			for module in modules:
				self.pymerio.pause_module( module )
	def do_resume( self, line ):
		'''Resume module'''
		if( line.strip() == '' and self.module != '' ):
			self.pymerio.resume_module( self.module )
		elif( line.strip() == '' ):
			self.error( 'No module selected' )
		else:
			if( line.strip() == 'all' ):
				modules = self.pymerio.paused[:]
				if( len( modules ) == 0 ):
					self.error( 'No modules to resume.' )
			else:
				modules = shlex.split( line )
			for module in modules:
				self.pymerio.resume_module( module )

	def do_banner( self, line ):
		'''Show banner'''
		self.out( '\n'+self.get_banner()+'\n' )
	def do_count( self, line ):
		'''Show module count'''
		self.out( self.get_groups() )
		self.out( self.get_count() )
	def get_banner( self ):
		return  open(os.path.join(  os.path.abspath(os.path.dirname(__file__)),
									'..',
									'misc',
									'banners',
									'banner' ),
					 'r' ).read()
	def get_count( self ):
		return ' %s[%d]%s total modules%s'%(self.B, 
											len( self.pymerio.modules.keys() ),
											self.O, 
											self.N )
	def get_groups( self ):
		categories = {}
		for module in self.pymerio.modules.keys():
			category, name = module.split( '::' )
			if( category not in categories.keys() ):
				categories[ category ] = 0
			categories[ category ] = categories[ category ] + 1
		formatter = ' %s[%d]%s %s modules%s\n'
		ret_str = ''
		for category in categories.keys():
			ret_str = ret_str + formatter % (   self.B,
												categories[ category ],
												self.O,
												category,
												self.N )
		return ret_str[:-1]
	def do_version( self, line ):
		'''Show version'''
		self.out( '' )
		self.out( self.get_copywrong() )
		self.out( '' )
	def get_copywrong( self ):
		return ' %spymerio v0.1a%s' % ( self.O, self.N )

	def do_interact( self, line ):
		'''Module interactive mode'''
		if( line.strip() == '' and self.module == '' ):
			self.error( 'No module selected.' )
			return
		modules = shlex.split( line )
		if( len(modules) == 1 ):
			self.pymerio.interact( modules[0] )
		else:
			self.pymerio.interact( self.module )

	def complete_interact( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.active.keys()
		else:
			completions = [ f for f in self.pymerio.active.keys() 
							if f.startswith( text ) ]
		return completions

	def help_interact( self ):
		print 'Usage: interact [module]'

	def do_reload( self, line ):
		'''Reload modules'''
		#restore_flag = self.pymerio.quiet_load
		#self.pymerio.quiet_load = True
		self.pymerio.load_modules()
		#self.pymerio.quiet_load = restore_flag

	def do_back( self, line ):
		'''Deselect module or exit'''
		if( self.module != '' ):
			self.module = ''
			self.prompt = self._prompt % ( self.pymerio.g_opts['session'], '' )
	def do_exit( self, line ):
		'''Exit'''
		self.pymerio.quit()
		sys.exit()

	def complete_run( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.modules.keys()
		else:
			completions = [ f for f in self.pymerio.modules.keys() 
							if f.startswith( text ) ]
		return completions
	def complete_use( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.modules.keys()
		else:
			completions = [ f for f in self.pymerio.modules.keys() 
							if f.startswith( text ) ]
		return completions
	def complete_stop( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.active.keys()
			if( len( completions ) > 0 ):
				completions.append( 'all' )
		else:
			completions = [ f for f in self.pymerio.active.keys() 
							if f.startswith( text ) ]
			if( len( self.pymerio.active.keys() ) > 0 ):
				if( 'all'.startswith( text ) ):
					completions.append( 'all' )
		return completions
	def complete_pause( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.active.keys()
			if( len( completions ) > 0 ):
				completions.append( 'all' )
		else:
			completions = [ f for f in self.pymerio.active.keys() 
							if f.startswith( text ) ]
			if( len( self.pymerio.active.keys() ) > 0 ):
				if( 'all'.startswith( text ) ):
					completions.append( 'all' )
		return completions
	def complete_resume( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.paused[:]
			if( len( completions ) > 0 ):
				completions.append( 'all' )
		else:
			completions = [ f for f in self.pymerio.paused
							if f.startswith( text ) ]
			if( len( self.pymerio.paused[:] ) > 0 and 'all'.startswith( text )):
					completions.append( 'all' )
		return completions

	def complete_cd( self, text, line, begidx, endidx ): # it'll do for now
		if not text:
			completions = [ f for f in os.listdir( './' )
							if os.path.isdir( f ) ]
		else:
			completions = [ f for f in os.listdir( './' )
							if os.path.isdir( f ) and f.startswith( text ) ]
		return completions

	def do_help( self, arg ):
		'''Show this message'''
		cmd.Cmd.do_help( self, arg )
	def print_topics(self, header, cmds, cmdlen, maxcol): 
		if cmds and header == self.doc_header:
			self.out( ' %s' % ( str( header ) ))
			if self.ruler:
				self.out( ' %s' % str( self.ruler * len( header ) ) )
			for cmd in cmds:
				if( cmd == 'EOF' or cmd == 'cd' ):
					continue
				self.out(' %s %s' % (   cmd.ljust(15),
										getattr(self,'do_'+cmd).__doc__) )
			self.out( '' )

	def do_watch( self, line ):
		'''Watch for module[s] output'''
		modules = shlex.split( line )
		if( len(modules) == 0 ):
			if( self.module == '' ):
				modules=[ 'all' ] # no module in use, watch all modules
			else:
				modules=[ self.module ] # watch module in use
		self.info( 'Press ^C to exit.' )
		for module in modules:
			if( module not in self.pymerio.modules.keys() and module != 'all' ):
				self.error( 'Module %s not loaded' % ( module ) )
				return
		try:
			output.OutputModule._monitor.set()
			while( True ):
				output.OutputModule._output_add.wait( 86400 ) # wtf ? DNR!!!
				if( output.OutputModule._output_add.isSet()
						and ( output.OutputModule._modified in modules
								or modules[0] == 'all' )):
					id = output.OutputModule._mid
					log = self.db.getLastLog( _id = id ) # race condition fixed
					if( len( modules ) == 1 and modules[0] != 'all' ):
						self.print_log( log ) # one module only
					else:
						self.print_log( log, True ) # diffirentiate
				output.OutputModule._output_add.clear()
				output.OutputModule._output_fin.set()
		except KeyboardInterrupt:
			self.out( '' )
			self.info( 'Returning to shell..' )
		except:
			import traceback; traceback.print_exc()
			self.error( 'Failed during watching' )
		finally:
			output.OutputModule._monitor.clear()

	def help_watch( self ):
		print 'Usage: watch [module1] [module2] [..]'
	def complete_watch( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.active.keys()
			completions.append( 'all' )
		else:
			completions = [ f for f in self.pymerio.active.keys()
							if f.startswith( text ) ]
			if( 'all'.startswith( text ) ):
				completions.append( 'all' )
		return completions

	def do_output( self, line ): # KISS, SM00CH
		'''Print module[s] output'''
		lines = shlex.split( self.module + ' ' + line )
		kw = {}
		regex = {}
		regex['module']=re.compile( 
				r"^(?P<val>[a-z,A-Z,0-9,_-]+::[a-z,A-Z,0-9,_-]+)$",
				re.U )
		regex['offset']=re.compile( r"^(?:\d+)?:(?P<val>\d+)$", re.U )
		regex['size']=re.compile( r"^(?P<val>\d+)(?::\d+)?$", re.U )
		regex['all']=re.compile( r"^(?P<val>all)$", re.U )
		regex['global']=re.compile( r"^(?P<val>global)$", re.U )
		for line in lines[:]:
			for param,rgx in regex.items():
				r = re.match( rgx, line )
				if( r ):
					kw[param] = r.groups('val')[0]
		if( 'all' in kw.keys() ):
			kw['all'] = True
		if( 'global' in kw.keys() ):
			if( 'module' in kw.keys() ):
				del kw['module']
			del kw['global']
		self.print_logs( **kw )

	def print_logs( self, **kwargs ):
		if( 'module' in kwargs.keys() 
				and kwargs['module'] not in self.pymerio.modules.keys() ):
			self.error( 'Module %s not loaded' % ( kwargs['module'] ) )
			return
		logs = self.db.getLogs( **kwargs )
		for log in logs:
			if( 'module' not in kwargs.keys() ):
				self.print_log( log, True )
			else:
				self.print_log( log )
		if( logs.count() == 0 ):
			if( 'module' in kwargs.keys() ):
				self.error( 'Module %s has no output' % ( kwargs['module'] ) )
			else:
				self.error( 'No output' )
	def print_log( self, log, show_mod = False ):
		if( show_mod ):
			module = mongo.getModule( log )
		else:
			module = None
		message = mongo.getMessage( log )
		format = mongo.getFormat( log )
		if( format == 'info' ):
			self.info( message, module )
		elif( format == 'error' ):
			self.error( message, module )
		elif( format == 'trace' ):
			self.trace( message, module )
		elif( format == 'warn' ):
			self.warn( message, module )
		elif( format == 'success' ):
			self.success( message, module )
		else: # unkown log type
			self.unknown( message, module )

	def help_output( self ):
		print 'Usage: output [module|global] [amount[:offset]|all]'
	def complete_output( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.modules.keys()
			completions.append( 'all' )
			completions.append( 'global' )
		else:
			completions = [ f for f in self.pymerio.modules.keys()
							if f.startswith( text ) ]
			if( 'all'.startswith( text  ) ):
				completions.append( 'all' )
			if( 'global'.startswith( text ) ):
				completions.append( 'global' )
		return completions
	def help_errors( self ):
		print 'Usage: errors [module] [amount] [offset]'
	def complete_errors( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.modules.keys()
			completions.append( 'all' )
		else:
			completions = [ f for f in self.pyermio.modules.keys()
							if f.startswith( text ) ]
			if( 'all'.startswith( text  ) ):
				completions.append( 'all' )

	def do_greetz( self, line ):
		self.out( 	'''%s%s\n %s
 To everyone writing opensource.
\n%s%s%s''' % ( self.B, '-'*80, self.O, self.B, '-'*80, self.N ) )

	def do_modules( self, line ):
		'''Show all loaded modules'''
		for module in self.pymerio.modules.keys():
			self.info( module )
	
	def help_modules( self, line ):
		print 'Usage: modules'

	def help_run( self ):
		print '''Usage: run [module]'''
	def help_stop( self ):
		print '''Usage: stop [module|all]'''
	def help_resume( self ):
		print '''Usage: resume [module|all]'''
	def help_pause( self ):
		print '''Usage: pause [module|all]'''
	def help_back( self ):
		print '''Usage: back'''
	def help_banner( self ):
		print '''Usage: banner'''
	def help_use( self ):
		print '''Usage: use <module>'''
	def help_count( self ):
		print '''Usage: count'''
	def help_help( self ):
		print '''Usage: help [command]'''
	def help_exit( self ):
		print '''Usage: exit'''
	def help_py( self ):
		print '''Usage: py <command>'''
	def help_reload( self ):
		print '''Usage: reload'''

	def do_set( self, line ):
		'''Set variable'''
		try:
			key, val = shlex.split( line )
		except:
			self.help_set()
			return
		self.pymerio.set_opt( key, val, self.module )
	def do_unset( self, line ):
		'''Unset variable'''
		lines = shlex.split( line )
		module = self.module
		for key in lines:
			try:
				if( ( module not in self.pymerio.opts.keys()
						or key not in self.pymerio.opts[ module].keys() )
						and '' in self.pymerio.opts.keys()
						and key in self.pymerio.opts[''].keys() ):
					# unset global option from local context
					self.pymerio.unset_opt( key, '' )
				else:
					# unset local option from local context
					self.pymerio.unset_opt( key, self.module )
			except:
				self.warn( 'Option %s does not exist.' % ( key ) )
	def help_set( self ):
		print 'Usage: set <key> <val>'
	def help_unset( self ):
		print 'Usage: unset <key>'
	def complete_set( self, text, line, begidx, endidx ):
		completions = []
		if not text:
			if( self.module in settings.Settings.opts.keys() ):
				completions = settings.Settings.opts[self.module].keys()
			if( self.module in self.pymerio.opts.keys() ):
				completions += self.pymerio.opts[self.module].keys()
		else:
			if( self.module in settings.Settings.opts.keys() ):
				completions = [ f 
						for f in settings.Settings.opts[self.module].keys()
						if f.startswith( text ) ]
			if( self.module in self.pymerio.opts.keys() ):
				completions += [ f
						for f in self.pymerio.opts[ self.module ].keys()
						if f.startswith( text ) ]
		return completions
	def complete_unset( self, text, line, begidx, endidx ):
		completions = []
		if( self.module != '' 
				and self.module not in settings.Settings.opts.keys() ):
			return []
		if not text:
			if( self.module in self.pymerio.opts.keys() ):
				completions = self.pymerio.opts[ self.module ].keys()
			if( '' in self.pymerio.opts.keys() 
					and self.module != '' ): # append global options
				completions += [ f 
						for f in self.pymerio.opts[''].keys()
						if f in settings.Settings.opts[ self.module ].keys() ]
		else:
			if( self.module in self.pymerio.opts.keys() ):
				completions = [ f 
						for f in self.pymerio.opts[self.module].keys()
						if f.startswith( text ) ]
			if( '' in self.pymerio.opts.keys() 
					and self.module != '' ): # append global options
				completions += [ f
						for f in self.pymerio.opts[''].keys()
						if f.startswith( text )
						and f in settings.Settings.opts[ self.module ].keys() ]
		return completions

	def do_options( self, line ): # there be dragons here
		# global opts override default opts
		# local opts override global opts
		if( self.module != '' ):
			module = self.module
			option = line.strip()
			if( option != '' ): # output for [ module ] options <option>
				self.info( 'Name: %s' % ( 
						settings.Settings.opts[module][option]['name'] ) )
				if(settings.Settings.opts[module][option]['required'] is True ):
					self.info( 'Required: %s' % ( 
							settings.Settings.opts[module][option]['required']))
				# start options value block
				if( module in self.pymerio.opts.keys()
						and option in self.pymerio.opts[ module ].keys() ):
					self.info( 'Local: %s' % (
						self.pymerio.opts[ module ][ option ] ))
				if( '' in self.pymerio.opts.keys()
						and option in self.pymerio.opts[''].keys() ):
					self.info( 'Global: %s' % (
						self.pymerio.opts[''][option] ))
				self.info( 'Default: %s' % ( 
					settings.Settings.opts[module][option]['default'] ))
				# end options value block
				self.info( 'Description: %s' % ( 
						settings.Settings.opts[module][option]\
								['description'].replace('\t','     ' )))#format
				self.info( 'Comment: %s' % ( 
						settings.Settings.opts[module][option]\
								['comment'].replace('\t','     ' ) ) )#format
				return
			# output for [ module ] options
			if( module not in settings.Settings.opts.keys() ):
				self.warn( 'Module %s has no options' % ( module ) )
				return
			for opt in settings.Settings.opts[module]:
				if( module in self.pymerio.opts.keys() 
						and opt in self.pymerio.opts[module].keys() ):
					self.info( '%s => %s' %(opt,self.pymerio.opts[module][opt]))
				elif( '' in self.pymerio.opts.keys()
						and opt in self.pymerio.opts[''].keys() ):
					self.info( '%s => %s' % ( opt,
						self.pymerio.opts[''][opt] ))
				elif( settings.Settings.opts[module][opt]['required'] is True ):
					self.warn( '%s option required.' % ( opt ) )
				else:
					self.info( '%s => %s' % (opt,settings.Settings.opts[module]\
							[opt]['default'] ) ) # show default values
		else: 
			module = line.strip()
			if( module != '' ):
				# output for [] options <module>
				if( module not in self.pymerio.modules.keys() ):
					self.error( 'Module %s not loaded' % ( module ) )
					return
				if( module in settings.Settings.opts.keys() ):
					for opt in settings.Settings.opts[ module ].keys():
						# start options value block
						if( '' in self.pymerio.opts.keys()
								and opt in self.pymerio.opts[''].keys() ):
							self.info( '%s => %s' % ( opt,
								self.pymerio.opts[''][opt] ))
						elif( module in self.pymerio.opts.keys()
								and opt in self.pymerio.opts[module].keys()):
							self.info( '%s => %s' % ( opt,
								self.pymerio.opts[ module ][ opt ] ))
						elif( settings.Settings.opts[module][opt]['required'] \
								is True ):
							self.warn('%s option required.' % ( opt ) )
						else:
							self.info( '%s => %s' % ( opt,
								settings.Settings.opts[module][opt]['default']))
						# end options value block
					return
				else:
					self.error( 'Module %s has no options' % ( module ) )
					return
			# output for [] options
			if( '' in self.pymerio.opts.keys() 
					and len( self.pymerio.opts[''].keys() ) > 0 ):
				for opt in self.pymerio.opts[''].keys():
					self.info( '%s => %s' % ( opt, self.pymerio.opts[''][opt] ))
			else:
				self.info( 'No options set' )
	def help_options( self ):
		if( self.module == '' ):
			print 'Usage: options [module]'
		else:
			print 'Usage: options [option]'
	def complete_options( self, text, line, begidx, endidx ):
		if( self.module == '' ): # complete modules
			if not text:
				completions = self.pymerio.modules.keys()
			else:
				completions = [ f for f in self.pymerio.modules.keys()
								if f.startswith( text ) ]
			return completions
		else: # complete options
			if not text:
				completions = settings.Settings.opts[self.module].keys()
			else:
				completions = [ f 
							for f in settings.Settings.opts[self.module].keys()
							if f.startswith( text ) ]
			return completions

	def do_status( self, line ):
		'''Show status of modules'''
		if( line in self.pymerio.modules.keys() ):
			if( line in self.pymerio.paused[:] ):
				self.info( 'Module %s => PAUSED' % ( line ) )
			elif( line in self.pymerio.active.keys() ):
				self.info( 'Module %s => RUNNING' % ( line ) )
			else:
				self.info( 'Module %s => STOPPED' % ( line ) )
		elif( line.strip() == '' ):
			self.out( ' %sRUNNING:%s' % ( self.O, self.N ) )
			running = [ f for f in self.pymerio.active.keys() 
					if f not in self.pymerio.paused[:] ]
			for module in running:
				self.info( '%s' % ( module ) )
			if( len( running ) == 0 ):
				self.out( ' %s    None%s' % ( self.O, self. N ) )
			self.out( ' %sPAUSED:%s' % ( self.O, self.N ) )
			for module in self.pymerio.paused[:]:
				self.info( '%s' % ( module ) )
			if( len( self.pymerio.paused ) == 0 ):
				self.out( ' %s    None%s' % ( self.O, self. N ) )
		else:
			self.error( 'Module %s not loaded' % ( line ) )
	def help_status( self ):
		print 'Usage: status [module]'
	def complete_status( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.modules.keys()
		else:
			completions = [ f for f in self.pymerio.modules.keys()
							if f.startswith( text ) ]
		return completions

	def modules( self, line ):
		'''Show loaded modules'''
		for module in self.pymerio.modules.keys():
			self.info( '%s' % ( module ) )
	def help_modules( self ):
		print 'Usage: modules'


	def do_info( self, line ):
		'''Show info about module'''
		module = line.strip()
		if( module == '' and self.module != '' ):
			module = self.module
		if( module not in self.pymerio.modules.keys() ):
			self.error( 'Module %s not loaded.' % ( module ) )
			return
		if(	module not in settings.Settings.infos.keys() ):
			self.error( 'Module %s does not have any info.' % ( module ) )
			return
		self.info( 'Name: %s' % ( settings.Settings.infos[module]['name'] ) )
		self.info( 'Author: %s' % ( settings.Settings.infos[module]['author'] ))
		self.info('Description: %s' % (
				settings.Settings.infos[module]\
						['description'].replace('\t','     ' ) ) ) #format
		self.info( 'Comments: %s'%( settings.Settings.infos[module]\
				['comment'].replace('\t','     '))) #format
	def help_info( self ):
		print 'Usage: info [module]'
	def complete_info( self, text, line, begidx, endidx ):
		if not text:
			completions = self.pymerio.modules.keys()
		else:
			completions = [ f for f in self.pymerio.modules.keys()
							if f.startswith( text ) ]
		return completions

	def do_script( self, line ):
		'''Run script from scripts folder'''
		threading.Thread( target = ShellBase.script_thread, args=( self, line ) ).start()

	def script_thread( self, line ):
		scripts = shlex.split( line )
		for script in scripts:
			try:
				f=open(os.path.join(os.path.abspath(os.path.dirname(__file__)),
									'..',
									'scripts',
									script ) )
				lines = f.readlines()
				try:
					for line in lines:
						self.onecmd( line )
				except:
					self.warn( 'Error running: %s in script %s' % ( line[:-1],
																	script ) )
			except:
				self.warn( 'Script %s does not exist.' % ( script ) )
	def help_script( self ):
		print 'Usage: script <name>'
	def complete_script( self, text, line, begidx, endidx ):
		completions = []
		path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
							'..',
							'scripts' )
		for dirpath, dirnames, filenames in os.walk( path ):
			if( len( filenames ) > 0 ):
				for file in filenames:
					if( file.endswith( '.x' ) ):
						if not text:
							completions.append( file )
						else:
							if file.startswith( text ):
								completions.append( file )
		return completions

	def do_load( self, line ):
		'''Load session or create new one'''
		lines = shlex.split( line )
		if( len( lines ) != 1 ):
			self.help_load()
			return
		else:
			self.pymerio.g_opts[ 'session' ] = lines[0]
		self.db.setSession( lines[0] )
		if( self.module == '' ):
			mod = ''
		else:
			mod = ' ' + self.module + ' '
		self.prompt = self._prompt % ( 	self.pymerio.g_opts[ 'session' ], mod )
	def help_load( self ):
		print 'Usage: load <session>'
	def complete_load( self, text, line, begidx, endidx ):
		if not text:
			completions = self.db.getSessionNames()
		else:
			completions = [ f for f in self.db.getSessionNames()
							if f.startswith( text ) ]
		return completions

	def do_saveas( self, line ):
		'''Save current session under another name'''
		lines = shlex.split( line )
		if( not lines or len( lines ) != 1 ):
			self.help_saveas()
			return
		session = self.pymerio.g_opts[ 'session' ]
		new = lines[0]
		try:
			self.db.cloneSession( session, new )
			self.info( 'Saved session [ %s ] as %s' % ( session, new ) )
		except:
			self.warn( 'Could not save session [ %s ] as %s' % ( session, new ))
	def help_saveas( self ):
		print 'Usage saveas <name>'
	def complete_saveas( self, text, line, begidx, endidx ):
		return self.db.getSessionNames()

	def do_sessions( self, line ):
		'''List sessions'''
		sessions = self.db.getSessionNames()
		if( len( sessions ) == 0 ):
			self.warn( 'No sessions available.' )
		for scan in sessions:
			self.info( '%s' % ( scan ) )
	def help_sessions( self ):
		print 'Usage: sessions'

	def do_delete( self, line ):
		'''Delete session'''
		scans = shlex.split( line )
		if( not scans ):
			self.help_delete()
			return
		if( ( len( scans ) == 1 and scans[0] == 'all' ) ):
			scans = self.db.getSessionNames()
		for scan in scans:
			try:
				self.db.delSession( scan )
				self.info( 'Deleted session [ %s ]' % ( scan ) )
			except:
				self.warn( 'Could not delete session [ %s ]' % (scan ) )
	def help_delete( self ):
		print 'Usage: delete <session|all>'
	def complete_delete( self, text, line, begidx, endidx ):
		completions = self.db.getSessionNames()
		if not text:
			completions.append( 'all' )
		else:
			completions = [ f for f in completions[:]
							if f.startswith( text ) ]
			if( 'all'.startswith( text ) ):
				completions.append( 'all' )
		return completions

	def do_target( self, line ):
		'''Set targets'''
		lines = shlex.split( line )
		if( not lines ):
			self.help_target()
			return
		self.db.setSession( self.pymerio.g_opts[ 'session' ] )
		for target in lines:
			r = re.match( '^([0-9]{1,3}(?:-[0-9]{1,3})?\.){3}'
							'[0-9]{1,3}(?:-[0-9]{1,3})?$',
							target,
							re.UNICODE )
			if( r ): # ip range
				self.db.insert_range( target )
			else:
				file = os.path.isfile( target )
				if( file ): # parse text file
					f = open( target, 'rb' ) 
					for target in f:
						self.do_target( target ) # recursion
					f.close()
				else: # default to domain
					self.db.domain( target )

	def help_target( self ):
		print 'Usage: target <domain.tld|ip range|file>'

	def do_wait( self, line ):
		'''Wait for module[s] to finish'''
		lines = shlex.split( line )
		if( self.module != '' ):
			modules = [ self.module, ]
		else:
			modules = lines
			if( len( modules ) == 0 ):
				self.error( 'No modules specified' )
				return
			for module in modules:
				if( module not in self.pymerio.modules.keys() ):
					self.error( 'Module %s not loaded' % ( module ) )
					return
				if( module not in self.pymerio.active.keys() ):
					self.error( 'Module %s not running' % ( module ) )
					return
		try:
			while( True ):
				if( len( [ x for x in modules 
					if x in self.pymerio.active.keys() ] ) == 0 ):
					break
				time.sleep( 1 ) # yeah, i know, but it's simple
		except:
			self.out('')

	def help_wait( self ):
		print 'Usage: wait [module]'

	#def do_search:
	#def do_grep:
	#def do_loot:
	#def do_query:

class Shell( ShellBase ):
	def __init__( self, pymerio ):
		self.pymerio = pymerio
		self.color( True )
		self._prompt = ' ~%s[%s]$ '
		self.intro = '\n' + self.get_banner() + '\n\t\t' + \
							self.get_copywrong() + '\n\n\n' + \
							self.get_groups() + '\n' + \
							self.get_count() + '\n'
		ShellBase.__init__( self )

class Interpretor( ShellBase ):
	def __init__( self, pymerio ):
		self.pymerio = pymerio
		self.color( False )
		prompt = ''
		use_rawinput = False
		ShellBase.__init__( self )
