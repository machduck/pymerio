#!/usr/bin/python

import mongo
import sys
import traceback

class SingletonDB():
	try:
		db = mongo.MongoDB()
	except:
		print '-'*80
		print ''
		traceback.print_exc()
		print ''
		print '-'*80
		print ''
		print ' [!] MongoDB error.'
		print ''
		sys.exit()
	@staticmethod
	def getInstance():
		return SingletonDB.db
