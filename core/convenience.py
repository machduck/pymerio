#!/usr/bin/python

import shlex
import urllib
import hashlib

class Convenience():
	'''Contains commandline convenience functions. Inherited by Shell'''
	def do_strtohex( self, line ):
		'''Convert string to hex'''
		parts = shlex.split( line )
		seperator = ''
		if( len( parts ) > 2 or len( parts ) == 0):
			self.out( 'Usage: strtohex <string> [seperator]' )
		if( len( parts ) == 2 ):
			seperator = parts[1]
		try:
			hexstr = parts[0].encode('hex')
			hexstr = seperator.join([ hexstr[i:i+2] 
									for i in range(0, len( hexstr), 2 )] )
		except:
			self.error( 'Could not convert to hex: %s' % ( parts[0] ) )
			return
		self.out( '%s%s' % ( seperator, hexstr ))
	def help_strtohex( self ):
		print 'Usage: strtohex <string> [seperator]'

	def do_hextostr( self, line ):
		'''Convert hex to string'''
		parts = shlex.split( line )
		seperator = ''
		if( len( parts ) > 2 or len( parts ) == 0):
			self.out( 'Usage: hextostr <string> [seperator]' )
		if( len( parts ) == 2 ):
			seperator = parts[1]
		try:
			str = parts[0].replace( seperator, '' )
			str = str.decode('hex')
		except:
			self.error( 'Could not convert to str: %s' % ( parts[0] ) )
			return 
		self.out( '%s' % ( str ))
	def help_hextostr( self ):
		print 'Usage: hextostr <string> [seperator]'

	def do_urlencode( self, line ):
		parts = shlex.split( line )
		for part in parts:
			try:
				self.info( '%s' % ( urllib.quote_plus( part ) ) )
			except:
				self.warn( 'Could not urlencode: %s' % ( part ) )
	def help_urlencode( self ):
		print 'Usage: urlencode <string>'

	def do_urldecode( self, line ):
		parts = shlex.split( line )
		for part in parts:
			try:
				self.info( '%s' % ( urllib.unquote_plus( part ) ) )
			except:
				self.warn( 'Could not urldecode: %s' % ( part ) )
	def help_urldecode( self ):
		print 'Usage: urldecode <string>'
