#!/usr/bin/python

import sys
import os
import imp
import traceback
import output
import settings
import db
import re

class Pymerio( output.Output ):
	def __init__( self ):
		self.color( True )
		self.modules = {}
		self.active = {}
		self.paused = []
		self.opts = {}
		self.g_opts = {}
		self.g_opts[ 'session' ] = 'default'
		self.quiet_load = 0
		self.seperator = '::'
		self.loaded = 0
		try: # create db stuff
			db.SingletonDB.getInstance().install()
			db.SingletonDB.getInstance().scan( self.g_opts[ 'session' ] )
		except:
			self.trace()
			self.out('')
			self.error( 'Error connecting to database, check settings.conf' )
			self.out('')
			sys.exit()

	def load_modules( self ):
		if( self.loaded ):
			self.info( 'Reloading modules..' )
			self.quit()
			self.modules.clear() # reset all running modules
			self.active.clear()
			del self.paused[:]
		else:
			self.loaded = True
			if( not self.quiet_load ):
				self.out( '' )
		for dirpath, dirnames, filenames in os.walk( './modules' ):
			if( len( filenames ) > 0 ):
				for file in filenames:
					if( file.endswith( '.py' ) ):
						filepath = os.path.join( dirpath, file )
						module = file[:-3];
						try:
							path, dir = os.path.split( dirpath )
							module = dir + self.seperator + module
							settings.Settings.set_mod( module )  # settings !
							self.modules[ module ] = imp.load_source(module, 
																	 filepath )
							if( not self.quiet_load ):
								self.info( 'Loaded module: %s'%( module ) )
						except:
							if( not self.quiet_load ):
								self.warn('Module %s failed to load.'%(module))
								self.trace()
	def trace( self ):
		print '-'*80
		print self.R
		traceback.print_exc( file=sys.stderr )
		print self.N
		print '-'*80
	def run_module( self, name ):
		'''run module'''
		if( name == '' ):
			self.error( 'No module selected.' )
			return
		if( name not in self.modules.keys() ):
			self.error( 'Module %s not loaded.' % ( name ) )
			return
		if( name in self.active.keys() ):
			self.warn( 'Module %s already running.' % ( name ) )
			return
		tmp = {}
		if( name in settings.Settings.opts ): # some modules have no settings
			for opt in settings.Settings.opts[name]:
				if( settings.Settings.opts[name][opt]['required'] is True
					and ( name not in self.opts.keys() 
					or opt not in self.opts[name].keys() ) ):
					self.error( 'Module requires option: %s' % ( opt ) )
					return
				else:
					if( name in self.opts.keys() 
							and opt in self.opts[name].keys() ): 
						# set overriden value
						tmp[settings.Settings.opts[name][opt]['name']] = \
								self.opts[name][opt]
					else:
						if( '' in self.opts.keys() 
								and opt in self.opts[''].keys() ):
							# inject global option
							# global variables override default,
							# but are overriden by local
							# yes, global opts have '' key, dont ask
							tmp[ opt ] = self.opts[''][opt]
						else:
							# set default value
							tmp[settings.Settings.opts[name][opt]['name']] = \
									settings.Settings.opts[name][opt]['default']
			tmp = settings.Extract( tmp )
		plugin = False # plugin flag
		try:
			self.active[ name ] = self.modules[ name ].Module( name )
		except:
			try:
				self.active[ name ] = self.modules[ name ].Plugin( name )
				plugin = True
			except:
				self.warn( 'Module %s corrupt: class Module or Plugin '\
						'not found.' % ( name ) )
				self.trace()
				if( name in self.active.keys() ):
					del self.active[ name ]
				return
		self.active[ name ].option = tmp
		db.SingletonDB.getInstance().scan( self.g_opts[ 'session' ] )
		self.active[ name ].module_exit = self.module_exit # hellz yeah!
		try:
			self.active[ name ]._start() # call base class proxy method
			if( plugin == True ): # if plugin, delete from active
				del self.active[ name ]
		except:
			self.warn( 'Module %s failed.' % ( name ) )
			self.trace()
			if( name in self.active.keys() ): # possible race condition
				del self.active[ name ]

	def module_exit( self, name ):
		try:
			del self.active[ name ]
		except:
			self.warn( 'Module %s failed attempting to stop.' % ( name ) )
			self.trace()

	def stop_module( self, name ):
		'''stop module'''
		if( name not in self.active.keys() ):
			self.error( 'Module %s not running.' % (name ) )
			return
		if( name in self.paused[:] ):
			self.paused.remove( name )
		try:
			self.active[ name ]._stop()
			if( name in self.active.keys() ): # possible race condition
				del self.active[ name ]
		except:
			self.warn( 'Module %s failed attempting to stop.' % ( name ) )
			self.trace()

	def pause_module( self, name ):
		'''pause module'''
		if( name in self.active.keys() and name not in self.paused[:] ):
			self.paused.append( name )
			try:
				self.active[ name ]._pause()
			except:
				self.paused.remove( name )
				self.warn( 'Module %s failed attempting to pause.' % ( name ) )
				self.trace()
			if( name not in self.active.keys() ):
				# if module finishes after if evaluates True
				self.paused.remove( name )
		else:
			self.error( 'Module %s not running.' % ( name ) )
	def resume_module( self, name ):
		'''resume module'''
		if name not in self.active.keys():
			self.error( 'Module %s not active.' % ( name ) )
			return
		if name not in self.paused[:]:
			self.error( 'Module %s not paused.' % ( name ) )
			return
		self.paused.remove( name )
		try:
			self.active[ name ]._resume()
		except:
			self.warn( 'Module %s failed attempting to resume.' % ( name ) )
			self.trace()
	def quit( self ):
		'''stop all modules'''
		for module in self.active.keys():
			self.stop_module( module )

	def set_opt( self, key, val, module ):
		if( module not in self.opts.keys() ):
			self.opts[ module ] = {}
		if( key in self.opts[ module ].keys() ):
			del self.opts[ module ][ key ]
		self.opts[ module ][ key ] = self.auto_convert( val )
	def unset_opt( self, opt, module ):
		del self.opts[ module ][ opt ]
	
	def set_g_opt( self, key, val ):
		if( key in self.g_opts.keys() ):
			del self.g_opts[ key ]
		self.g_opts[ key ] = self.auto_convert( val )
	def unset_g_opt( self, key ):
		if( key == 'session' ): # NO
			return
		del self.g_opts[ key ]

	def auto_convert( self, val ):
		r = re.match( '^(0x)?[0-9]+$', val )
		if( r ):
			if( r.groups()[0] is not None ):
				val = int( val, 16 )
			else:
				val = int( val )
			return val
		f = re.match( '^(-)?[0-9]+\.[0-9]+$', val )
		if( f ):
			val = float( val )
			return val
		return val

	def interact( self, module ):
		if( module not in self.modules.keys() ):
			self.error( 'Module %s not loaded.' % ( module ) )
			return
		if( module not in self.active.keys() ):
			self.error( 'Module %s not runnning.' % ( module ) )
			return
		try:
			self.modules[ module ].framework.Interact.__init__(
														self.active[ module ] )
			self.active[ module ].cmdloop()
		except( TypeError, AttributeError ):
			self.error( 'Module does not support interactive mode.' )
			return
		except KeyboardInterrupt:
			self.out( '' )
			self.info( 'Returning to shell..' )
			return
		except:
			self.warn( "Interactive mode failed for module: %s." % ( module ) )
			self.trace()
			return
