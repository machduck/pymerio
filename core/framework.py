#!/usr/bin/python

import threading
import os
import output
import traceback
import cmd
import settings
import db as dbase
import convenience
import hashlib
import sys
from libs import thread

__author__ = 'TODO'

class ValidOptionsCheck():
	def __init__( self, option = None, msg = None ):
		self.option = option
		self.msg = msg
	def __nonzero__( self ):
		if( self.option is None ):
			return True
		else:
			return False
	def __str__( self ):
		if( self.option is None ):
			return ''
		return self.option

class Base():
	def __init__( self ):
		pass
	def validate_options( self ):
		for opt in settings.Settings.opts[ self.name ].keys():
			if( settings.Settings.opts[ self.name ][ opt ]['default'] == True
					and opt not in self.option.keys() ):
				return ValidOptionsCheck( opt, 'not set' ) #return False
			if( not settings.Settings.opts[ self.name ][ opt ]['validate'](
				self.option[ opt ] # run test function
			) ):
				return ValidOptionsCheck( opt, 'wrong value' ) # return False
		return ValidOptionsCheck() #return True

class Plugin( output.Output, Base ):
	'''Module superclass that is to be used when there's no need for threads.'''
	def __init__( self, name='' ):
		self.color( True )
		self.name = name
		Base.__init__( self )
	def _start( self ):
		self.init()

class Module( threading.Thread, output.OutputModule, Base ):
	'''Module superclass automatically launches module in another thread.'''
	def __init__( self, name = '' ):
		self.color( True )
		threading.Thread.__init__( self )
		Base.__init__( self )
		self.daemon = True
		if( name != '' ):
			self.name = name
		self._finish = threading.Event()
		self._paused = threading.Event()
		self._resumed = threading.Event()
		self._inactive = threading.Event()

	def _start( self ):
		self.start()

	def _stop( self ):
		self._finish.set() 
		self._inactive.wait()
		self.stop()

	def _pause( self ):
		self._paused.set()
		self._finish.set() # interrupt wait
		self._inactive.wait()
		#self._finish.clear()
		self.pause()
	def _resume( self ):
		self._paused.clear()
		self._inactive.clear()
		self._finish.clear() # alpha
		self._resumed.set()
		self.resume()

	def active( self ):
		if( self._paused.isSet() ):
			self._inactive.set()
			self._resumed.wait()
		if( self._finish.isSet() and not self._paused.isSet() ):
			return 0x0
		else:
			return 0x1
	def sleep( self, time ):
		self._finish.wait( time )
	def run( self ): # safegaurds
		self._finish.clear()
		self._inactive.clear()
		self._resumed.clear()
		self._paused.clear()
		try:
			self.init()
			self._inactive.set() # after we mopped up, allow call to stop()
		except SystemExit:
			self._inactive.set()
		except:
			self._inactive.set()
			self.trace( traceback.format_exc() )
		# call injected pymerio.Pymerio.stop_module(..)
		self.module_exit( self.name ) # possible wierd stuff ?
	def exit( self ):
		sys.exit()

class Interact( cmd.Cmd, output.OutputColor, convenience.Convenience ):
	def __init__( self ):
		self.color( True )
		self.prompt = '[ %s ] ' % ( self.name )
		self.doc_header = 'Commands (type [help|?] <topic>):'
		self.undoc_header = ''
		self.misc_header = ''
		self.ruler = '-'
		self.nohelp = '%s[-]%s No help for command: %%s%s' % ( 	self.R, 
																self.O, 
																self.N )
		cmd.Cmd.__init__( self )
	def do_EOF( self, line ):
		print( '' )
		return True
	def emptyline( self ):
		pass 
	def default( self, line ):
		print( '%s[-]%s Command %s not found%s'%( self.R,self.O,line,self.N ))
	def completedefault( self, text, line, begidx, endidx ):
		pass 
	def do_shell( self, line ):
		print( '%s[-]%s Shell commands turned off in interactive mode%s' % (
																		self.R,
																		self.O,
																		self.N))
	def do_back( self, line ):
		'''Exit interactive mode'''
		return True
	def do_help( self, arg ):
		'''Show this message'''
		cmd.Cmd.do_help( self, arg )
	def do_exit( self, line ):
		'''Exit interactive mode'''
		print( '' )
		return True
	def help_exit( self ):
		print 'Usage: exit'
	def print_topics( self, header, cmds, cmdlen, maxcol ): 
		if cmds and header == self.doc_header:
			print( ' %s' % ( str( header ) ))
			if self.ruler:
				print( ' %s' % str( self.ruler * len( header ) ) )
			for cmd in cmds:
				if( cmd == 'EOF' or cmd == 'cd' ):
					continue
				print(' %s %s'%(cmd.ljust(15),getattr(self,'do_'+cmd).__doc__))
			print( '' )

def info( name = '', author = '', description = '', comment = '' ):
	'''framework proxy method'''
	settings.Settings.set_info( name, author, description, comment )

def option( name,
			default,
			description,
			comment='',
			validate=lambda x: True,
			required = False ):
	'''framework proxy method'''
	settings.Settings.set_opt( 	name, 
								default,
								description,
								comment,
								validate,
								required )

def db():
	return dbase.SingletonDB.getInstance()

def hash( html ):
	return hashlib.sha1( html ).hexdigest()
